# Inkscape Manuals

This project is dedicated to the creation and to editing manuals for [Inkscape](https://inkscape.org).

Each manual team can opt to write their manual on a different platform, or to create a folder in this repository for their manual.

The team can make use of the [issues section](https://gitlab.com/inkscape/inkscape-docs/manuals/issues) to track their progress and their members' responsibilities.
Everyone can create new issues/tasks.
Issues for each manual need to be tagged with the name of the manual they belong to.

## Inkscape Beginners' Guide

### What's the Goal of the Guide?

This book is meant as an introduction for people who are not yet familiar with Inkscape terminology, and don't want to hear long explanations about how something works 'under the hood', but rather just want to get started quickly.

It is - at least at its beginning - a translation of the book ['Initiation à Inkscape'](https://fr.flossmanuals.net/initiation-inkscape/_draft/_v/1.0/introduction/), written in French by Elisa de Castro Guerra.

### Where is the Book being Created?

#### Editing platform:

This book is created at [flossmanuals.net](http://write.flossmanuals.net/start-with-inkscape/_draft), which is a website that allows for collaborative editing of books about Free/Libre and Open Source Software. To learn more about the community that's behind it, please visit http://www.flossmanuals.org/about-floss-manuals-foundation..

If you need help with the Editing interface (called Booktype), you can find more information [in its manual](http://sourcefabric.booktype.pro/booktype-16-for-authors-and-publishers/).

#### Task list and task-focused discussion:

Task progress is being tracked [here](https://gitlab.com/inkscape/inkscape-docs/manuals/issues), with each issue tagged "Beginners' Guide".

#### Discussions of global interest:

Major topics can be discussed on the [inkscape-docs mailing list](https://sourceforge.net/p/inkscape/mailman/inkscape-docs/), which is also where status updates will be posted from time to time. New members are encouraged to say hi there, and state what part of the book they would like to help with.

#### Discussions when editing the book:

Anyone who edits the book can exchange short messages, files and snippets via chat with other users who are online at the same time. These discussions are not logged, and it's even possible that others will not see your message, when they are focused on their work. For important topics, please use one of the other two communication channels.

### How do I join?

To join the Beginners' Guide editors, [register at flossmanuals.net](http://write.flossmanuals.net/accounts/signin/), and [join the group](http://write.flossmanuals.net/groups/beginners-manual-for-inkscape/).

To find a task that you would like to help with, look for open tasks in the [issue tracker](https://gitlab.com/inkscape/inkscape-docs/manuals/issues), and add a comment on the task if you would like to take it on, or tackle a part of it. Only take on a task when you know you will be able to work on it. When you can no longer help with a task, please unassign yourself and let the other editors know in a comment, so someone else can pick up where you left.

Subscribe to the [inkscape-docs mailing list](https://sourceforge.net/p/inkscape/mailman/inkscape-docs/) for updates and discussions revolving around the manual (and other Inkscape documentation). This is also the place where you can introduce yourself to the team and where you can ask questions when you need help, or just info (or where you can answer other people's questions).

Currently, we have 5 book admins (Elisa, Brynn, CR, Jabier and Maren, aka Moini, (initiator and organizer). Admins can change the book settings and create release versions.

### What do I need to know before I start editing away?

**IMPORTANT:**  
When you're editing the book, other people cannot access the chapter you are currently working on. To make sure they can make edits when you're done and have left, please ALWAYS leave the editor page either by clicking on the 'Cancel' or on the 'Save' button. If you leave it by just closing the tab in your browser, the chapter will stay locked and inaccessible for other editors.

When you've got an idea that would introduce a major change in the book's structure, please confer with other editors on the mailing list to see what they think about this.

#### Writing Style:

When deciding how to phrase something, think of yourself when you started to use a vector editor, or think of a teen who you would like to introduce to the program, and who you would like to enjoy the process, and to be able to make nice drawings after reading the book.

Stay on topic, make it easy to understand, and keep the learning curve shallow. Write less rather than more. Be nice and encouraging to the reader. There exists another manual that provides the deep, technical details. We need to keep in mind that it will also be read by people whose native language is not English.

For consistent terminology, please refer to the [Inkscape Glossary](http://wiki.inkscape.org/wiki/index.php/Inkscape_glossary).

#### Inkscape Version:

This book refers to the Inkscape 0.92.x series (important for your screenshots and any description of functionality!).

#### Chapter status:

Chapters move from one status to the next, when they're being worked on:

1. needs translation:
Chapter isn't (fully) translated yet. If you speak both French and English, this is where your help is needed! This step only requires you to work on the text. You can ignore any images.

2. needs content:
Something is missing, the chapter needs someone who will add some more content text to it.

3. to be proofed:
Chapter is translated, and needs to be checked by someone other than the translator, to spot errors and do some polishing. If English is your native language and you're good with words, grab this job!

4. needs images:
The chapter contains images, but those are still French. Either it's just their file names that need to be changed (Workflow: download image, rename it on your computer, reupload, change link in the Chapter(s), provide meaningful alt text, delete French image) or a new screenshot is needed from the English interface. If you know how to do these things, please jump right in! (Hint: all image links follow this scheme: static/image_file.ext)

5. needs styling:
The chapter needs someone who will make it fit in better with other chapters - take a look at bolding, italics, consistent use of `<kbd>` tags for keyboard shortcuts, consistent use of styling for menu items, helpful hover texts for links, nice arrangement of images in the text.

6. completed:
This chapter is more or less finished. Concentrate your effort on the other ones  If you spot a mistake or typo, improvements are still possible, of course.

Don't forget to update a chapter's status, after you have worked on it.

### Under what Licence will the Book be Published?

This book is licensed under the [Creative Commons Attribution 4.0 license](CC-By 4.0, see https://creativecommons.org/licenses/by/4.0/).
By contributing to this book, you agree to publish your content under this license.

As a contributor, please add your name to the list in the 'About this Book' chapter.
This chapter will *always* be in need of new content ;-)

### Go!

Have fun working on the book at http://write.flossmanuals.net/start-with-inkscape/_edit/#