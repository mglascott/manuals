3D-Boxes
========

|Icon for 3D-Box Tool|

The **3D-Box** tool draws a (simulated) three-dimensional box. To
understand how it works, it helps to know a little bit about drawing in
perspective. This tool is of interest to people who want to draw
architecture or interiors. It makes it really easy to create furniture
or rooms, because it automatically creates perspective, by making use of
vanishing points.

To enable it, press the icon button shown above, or press [Shift]+[F4]
together, or [x].  After click-and-dragging, the box appears on the
canvas. This is best done somewhere near the center of the page area.
Else, the box can sometimes end up quite deformed. By default, two
vanishing points are placed in the vertical middle of the page borders,
one on the right, the other on the left.

When you look closely at the 3D-box that you have drawn, you will see
the following:

-  A small **black crosshair**, which designates the center of the box.
   When you click and drag it with your mouse, the box will move along
   with it, apparently in 3D space. Depending on its position in
   relation to the vanishing points, you will be able to see the box's
   bottom, or its top. If you move the box to the left or right, its
   shape will change in accordance with the vanishing lines.
-  Numerous (to be exact, 8) **white, diamond-shaped handles** allow you
   to extend or shrink the box along the x, y or z axis by dragging them
   with the mouse.
-  The **white, square-shaped handles** symbolize the vanishing points
   and can be moved in the same fashion as the other handles.

Note that these specific changes can only be achieved when you use the
3D-box tool. If you move the box with the Selection tool, both the box
and the vanishing points will move.

|Icon for Parallel Perspective (3D-Box)|

The tool controls for this tool allow you to set parameters for the 3
vanishing points for the x, y and z axis. The buttons with the above
icon depicting two parallel lines will make the vanishing lines parallel
and have an immediate effect on the box on the canvas.

 

 |The default 3D-Box|

Dark blue: the top of the box. Medium blue: its left side. Light blue:
its right side.

 

|Moving the 3D-Box|

Moving the box.

|Moving the vanishing points|

Moving the vanishing points.

|Additional vanishing point|

Adding a third vanishing point.

.. |Icon for 3D-Box Tool| image:: images/3d_box_tool.png
   :width: 50px
   :height: 52px
.. |Icon for Parallel Perspective (3D-Box)| image:: images/3d_box_parallel.png
   :width: 20px
   :height: 41px
.. |The default 3D-Box| image:: images/3d_box_default.png
   :width: 713px
   :height: 365px
.. |Moving the 3D-Box| image:: images/3d_box_moving.png
   :width: 724px
   :height: 467px
.. |Moving the vanishing points| image:: images/3d_box_moving_vanishing_points.png
   :width: 385px
   :height: 344px
.. |Additional vanishing point| image:: images/3d_box_adding_vanishing_point.png
   :width: 496px
   :height: 422px
