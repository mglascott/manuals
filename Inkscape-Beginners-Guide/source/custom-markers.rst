Customizing Markers
===================

Just as you can create custom patterns, you can also create your own,
custom markers. In order to do so, draw the marker you're envisioning,
then make it available for use by doing Object > Object to markers.

Your marker is now available at the top of the list of markers in the
drop-down menu.

|image0|

 

|example of a personalized marker|

 

|example of a personalized marker 2|

 

Markers are placed on the nodes of paths.

The dialog that gives you access to markers.

.. |image0| image:: images/personnalisersesmarqueurs.png
   :width: 619px
   :height: 401px
.. |example of a personalized marker| image:: images/personnalisersesmarqueurs_EN.png
   :width: 962px
   :height: 454px
.. |example of a personalized marker 2| image:: images/personnalisersesmarqueurs_2_EN.png
   :width: 854px
   :height: 435px
