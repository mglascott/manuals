Clipping and Masking
====================

These two features are both employed in the same way: Select the object
that you want to mask or clip, then the object that will serve as a mask
or clip, which must be positioned above the first object. Then use
Object > Clip > Set or Object > Mask > Set. The top object will then
hide parts of the bottom object.

The two features do not result in the exact same image, though. The clip
allows you to hide all parts of the bottom object that go beyond the
contours of the clip object. The colors of the mask object affect the
opacity of the bottom object.

|image0|

Clip

Mask

.. |image0| image:: images/decoupe-masque-w.png

