Installing Inkscape for Windows
===============================

Inkscape is available for Windows operating systems from XP up to
Windows 10.  There are two basic methods for installing Inkscape onto
computers with a Windows operating system.  One method is downloading
and installing directly from Inkscape and the other is installing it
from the Microsoft App store.  A description of both methods is
presented here.  **If you have previously installed Inkscape onto your
computer, you will need to uninstall that version before installing the
new version.**

Installation method 1: Downloading and installing directly from Inkscape
------------------------------------------------------------------------

1. Using a Web browser, go to the Inkscape page for downloading Inkscape
for Windows and select the option that matches the information you wrote
down in step 4 .

`https://inkscape.org/releases/ <https://inkscape.org/releases/>`_

 |image0|

2. Select an installation method from the 3 choices and wait for it to
download. You should either see a window giving you the option to save
the file or a pop-up will appear at the bottom of your web browser with
the file’s name and a timer stating how long until the download is
complete.

|image1|

3. One the download is complete either click on the file in the lower
left corner of your screen to start the installation process or, if need
be, go to your file explorer and open your downloads folder and select
the file from there. It should be the first file at the top of the
folder.

 |image2|

**OR**

 |image3|

 |image4|

 

 |image5|

4. If you get a User Account Control pop-up from Windows similar to the
following one click “OK” and wait for the Inkscape installation program
to start.

 |image6|

5. Select what language you want to use during the installation and
click “OK.” Then click “Next” on both the following welcome screen and
license agreement screen.

 |image7|

6. On the Choose Components screen you can select which feature you want
to install or not install. In most cases the default options should
provide all that the user needs, so click “Next.”  

 |image8|

7. On the Choose Install Location windows leave the destination folder
as C:\\Program Files\\Inkscape and click “Install” unless you want
Inkscape to be installed in a specific location on your computer.

 |image9|

(Optional step)

If you do want to install Inkscape in a specific location on your
computer, click the browse button and in the resulting window either
make a new folder for Inkscape and select it or select the destination
folder you want Inkscape to be installed to.

 |image10|

8.Once you click “Install” a progress bar will appear showing how long
it will take for your program to install.

9.After the installation is complete, click “Finish” and the installer
will automatically open Inkscape for you and you are ready to begin
working with Inkscape.

|image11|

Installation method 2: Installing from the Microsoft App store
--------------------------------------------------------------

You can also install Inkscape onto your Windows computer from the
Microsoft App store. If you have the Windows 10 Education version this
will be the only way you can install Inkscape. (Note: you will need a
Microsoft account to install apps from the Microsoft store. If you
already have an account, skip the following steps on how to make one.)

1. To create a Microsoft account using a Web browser go to
`https://login.live.com/ <https://login.live.com/>`_
 and select “Create one!”

 |image12|

2. On the following page enter in the email address or phone number that
you would like to link your Microsoft Account to and then click “Next.”
There is also the option to get a new email address from Microsoft.
(Note: I will be using preexisting email for this example but if you
want to create a new email the process for making a Microsoft email is
pretty straight forward.)

 |image13|

3. Enter a password that you wish to use with your new Microsoft account
and click “Next”.

 |image14|

4. Enter in your First and Last name then click “Next.”

 |image15|

5. Enter the Country/region in which you live and your Birthdate.

 |image16|

6. Go to the email you used to create the account and locate the email
from Microsoft to enter the code provided and click “Next.”

 |image17|

7. Enter your phone number and then click “send code” to receive the
security code, then click “Next.”

 |image18|

8. On the following page verify that the information is correct and
click “Looks good!” and your Microsoft Account is ready for use.

 |image19|

9. From the task bar at the bottom of your screen click the Microsoft
Store icon or enter store into the search bar and select Microsoft Store
from the results.

 |image20|

**OR**

 |image21|

10. Enter Inkscape into the Microsoft Store search bar and select
Inkscape from the results.

 |image22|

11. In the following windows to install Inkscape click “Get” and then
enter your Microsoft account username and then password in the windows
that pops up.

 |image23|

 |image24|

 |image25|

12. After signing into your Microsoft Account click “Install” and the
installation will begin.

 |image26|

13. Once the download and installation is complete click “Launch” and
you are ready to begin working with Inkscape.

 |image27|

Congratulations!  You have now installed Inkscape onto your Windows
computer. 

If you need further help, please visit `the FAQ section for Windows
specific problems <https://inkscape.ort/learn/faq/#windows-specific-issues>`_ on the Inkscape website.

Additional information
-----------------------

Identifying what type of Operating System you have
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before you can begin to download Inkscape, you must first know what type
of Operating System (OS) you have, 32-bit or 64-bit. If you already know
what type of Operating System you have please skip this section and go
the section on downloading Inkscape.

1. Click on the magnifying glass to open a search window and type
control panel.

 |image28|

2. Click to open the Control Panel.

3. On the open window select the “View By” option and change it from
“Category” to “Large Icons”, then scroll down until you see an icon that
says “System” and click on it.

 |image29|

4. Now on the window, toward the middle of the screen, you should see an
option that says “System Type”. Write down the associated information.
 You will need this during the download section of the installation.

 |image30|

Terms
~~~~~

Bit
  (short for binary digit) is the smallest unit of data in a computer. A bit has a single binary value, either 0 or 1.

32-bit architecture vs 64-bit architecture
  A 32-bit architecture means that the operating system is able to process data up to 32 bits in size. Likewise, a 64-bit architecture means an operating system can process data up to 64 bits. You need to use the correct program file for each architecture, otherwise the program won’t work properly.

  (Note: Most newer computers come as 64-bits. However, if you are not sure if your computer is 32-bits or 64-bits, we will cover how to look this up). 

Windows installer package files (.msi)
  An MSI file is a package that contains installation information for a particular installer, such as files to be installed and the locations for installation. They can be used for Windows updates as well as third-party software installers. The installation results are no different from using an executable file, but msi packages sometimes do have additional options such as doing silent installations (no human action needed) or pre-configured installs.

Executable (.exe)
  contains an executable program for Windows and is the standard file extension used by Windows programs. These are the files that make a program on your computer run.

Binary file (.bin)
  Binary files are files not meant to be read by humans. They contain no text but instead are composed of patterns 0s and 1s that can be directly read by the computer.

Binary Archive
  is a folder that contains multiple binary files and requires a special type of program to interact with them.

Zip (.zip)
  Zip files are a way to compress large amounts of data into a smaller, more manageable size. This allows for easy transportation and faster downloads of files. The disadvantage to zip files is that the files inside need to be extracted and decompressed before they can be used.

Portable app
  A portable app is a program that does not need to be installed onto a computer. Instead all necessary files needed to run the program reside in a single folder located on a disc or USB drive. The advantage to this is that the program can be taken anywhere the user goes and can be run from the cd or USB drive on any computer that supports the program. The disadvantages are that the program can easily be lost and if the USB isn’t properly removed from the computer after use, the program can get corrupted and stop working.


.. |image0| image:: images/InkScape4.png
.. |image1| image:: images/InkScape5.png
   :width: 1316px
   :height: 911px
.. |image2| image:: images/InkScape6.png
   :width: 1908px
   :height: 947px
.. |image3| image:: images/InkScape7.png
   :width: 310px
   :height: 76px
.. |image4| image:: images/InkScape8.png
   :width: 1919px
   :height: 393px
.. |image5| image:: images/InkScape9.png
   :width: 1914px
   :height: 441px
.. |image6| image:: images/InkScape10.2.png
   :width: 460px
   :height: 366px
.. |image7| image:: images/InkScape11.png
   :width: 362px
   :height: 193px
.. |image8| image:: images/InkScape12.png
   :width: 623px
   :height: 485px
.. |image9| image:: images/InkScape13.png
   :width: 623px
   :height: 485px
.. |image10| image:: images/InkScape14.png
   :width: 403px
   :height: 400px
.. |image11| image:: images/InkScape15.png
   :width: 623px
   :height: 485px
.. |image12| image:: images/InkScape16.png
.. |image13| image:: images/InkScape17.png
   :width: 1260px
   :height: 707px
.. |image14| image:: images/InkScape18.png
.. |image15| image:: images/InkScape19.png
.. |image16| image:: images/InkScape20.png
   :width: 747px
   :height: 696px
.. |image17| image:: images/InkScape21.png
.. |image18| image:: images/InkScape22.png
.. |image19| image:: images/InkScape23.png
   :width: 900px
   :height: 764px
.. |image20| image:: images/InkScape24.png
   :width: 429px
   :height: 45px
.. |image21| image:: images/InkScape25.png
   :width: 490px
   :height: 600px
.. |image22| image:: images/InkScape26.png
.. |image23| image:: images/InkScape27.png
   :width: 1913px
   :height: 852px
.. |image24| image:: images/InkScape28.png
   :width: 814px
   :height: 791px
.. |image25| image:: images/InkScape29.png
   :width: 814px
   :height: 791px
.. |image26| image:: images/InkScape30.png
.. |image27| image:: images/InkScape31.png
   :width: 1920px
   :height: 1030px
.. |image28| image:: images/InkScape1_1.png
   :width: 490px
   :height: 600px
.. |image29| image:: images/InkScape2.png
   :width: 1400px
   :height: 741px
.. |image30| image:: images/InkScape3.png
   :width: 1396px
   :height: 736px
