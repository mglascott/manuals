Copying and Duplicating Objects
===============================

The famous Copy keyboard shortcut :kbd:`Ctrl` + :kbd:`c` also works in Inkscape, but its brother Duplicate, with :kbd:`Ctrl` + :kbd:`d` is very useful, too.

Copying with :kbd:`Ctrl` + :kbd:`c` copies the selected element into the clipboard. It can then be pasted into the document by using the corresponding shortcut :kbd:`Ctrl` + :kbd:`v`. This inserts the object at the place of the cursor.

Duplicating, however, makes a copy of an object and positions this copy exactly on top of the original. This can be very useful when your copy needs to be in the same place as the original - you will not need to use the mouse or the alignment functions to move it back.
