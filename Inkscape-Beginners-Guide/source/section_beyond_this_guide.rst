Beyond This Guide
=================

Summary of what has been learnt

|
| Links to websites where one can get help

|
| Link to advanced manual and tutorials and other helpful sites.

|
| Some kind of nice message that encourages further exploring on one's
  own, experimenting, letting creativity flow :-)

.. toctree::

   test-chapter
