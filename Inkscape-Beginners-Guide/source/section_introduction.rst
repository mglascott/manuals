Introduction
============

This book was originally created by **Elisa de Castro Guerra**, designer and  `educational instructor <http://activdesign.eu/>`_. It is organized as double page spreads per topic, containing an explanation and an illustration, and was created as a reference guide for learning Inkscape. It is available under a Creative Commons license (`CC-By-SA <https://creativecommons.org/licenses/by/>`_) and is published on TO BE DETERMINED. The original French version can be obtained `here <https://www.flossmanualsfr.net/initiation-inkscape/>`_ .

The book was translated to English from French 2016-2017.

The version of the book that you are reading now has been updated and adapted by Inkscape community volunteers, to match Inkscape 0.92.2.

We sincerely hope you will find this little guide helpful when you are making your first steps with Inkscape, and that you will enjoy drawing in Inkscape just as we do. If you find something that could be improved, or needs to be updated, or have any other suggestions concerning this book, we'd be happy to have you join us at our `collaboration space at gitlab.com <https://gitlab.com/inkscape/inkscape-docs/manuals>`_ or on `Inkscape's documentation mailing list <https://sourceforge.net/p/inkscape/mailman/inkscape-docs/>`_

These are the people who contributed to this book:

- Jabiertxo Arraiza Cenoz
- brynn
- Elisa de Castro Guerra
- Sergio Gonz&aacute;lez Collado
- Hinerangi Courtenay
- Sylvain Chiron
- Maren Hachmann
- Rosalind Lord
- JP Otto
- Jon Peyer
- Christopher Michael Rogers
- Carl Symons
- Reidar Vik
- Marietta Walker
- *Please add your name, don't be shy :) (alphabetical order for family names)*

.. toctree::

   why-use-inkscape
   professional-software
