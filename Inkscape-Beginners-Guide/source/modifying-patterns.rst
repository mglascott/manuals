Modifying Patterns
==================

A pattern can easily be rotated, enlarged or shrunk. When the node tool
is active, an interesting trio will appear in the top left corner:

-  A cross, which you can use to move the pattern
-  A white, circular handle that allows you to rotate the pattern.
-  A white, square-shaped handle, which enlarges and shrinks the
   pattern.

When you grab these handles with the mouse and drag them, they will
affect the pattern, and the result will immediately be visible.

|image0|

Standard pattern with vertical stripes.

|image1|

The pattern has been rotated, using the circular handle.

|image2|

The pattern has been enlarged, by dragging the square handle.

.. |image0| image:: images/pattern02.png
   :width: 782px
.. |image1| image:: images/pattern03.png
   :width: 788px
.. |image2| image:: images/pattern04.png
   :width: 782px
