      <li><b>Introduction</b></li>
    
   
    
      <li><a href="../why-use-inkscape/index.html">Why use Inkscape?</a></li>
    
   
    
      <li><a href="../professional-software/index.html">Professional Software</a></li>
    
   
    
      <li><a href="../about-this-book/index.html">About this Book</a></li>
    
   
    
      <li><b>Installing Inkscape</b></li>
    
   
    
      <li><a href="../windows/index.html">Windows</a></li>
    
   
    
      <li><a href="../installing-inkscape/index.html">Mac OS X</a></li>
    
   
    
      <li><a href="../gnulinux/index.html">Gnu/Linux</a></li>
    
   
    
      <li><b>Getting Around in Inkscape</b></li>
    
   
    
      <li><a href="../interface/index.html">Interface</a></li>
    
   
    
      <li><a href="../managing-the-workspace/index.html">Managing the Workspace</a></li>
    
   
    
      <li><b>Drawing with Geometrical Shapes</b></li>
    
   
    
      <li><a href="../drawing-with-geometrical-shapes/index.html">Ways of Drawing with Inkscape</a></li>
    
   
    
      <li><a href="../shape-tools/index.html">Shape Tools</a></li>
    
   
    
      <li><a href="../select-tool/index.html">The Selector Tool</a></li>
    
   
    
      <li><a href="../squares-and-rectangles/index.html">Squares and Rectangles</a></li>
    
   
    
      <li><a href="../circles-ellipses-and-arcs/index.html">Circles, Ellipses and Arcs</a></li>
    
   
    
      <li><a href="../stars-and-polygons/index.html">Stars and Polygons</a></li>
    
   
    
      <li><a href="../spirals/index.html">Spirals</a></li>
    
   
    
      <li><a href="../3d-boxes/index.html">3D-Boxes</a></li>
    
   
    
      <li><b>Freehand Drawing</b></li>
    
   
    
      <li><a href="../freehand-drawing/index.html">Freehand Drawing</a></li>
    
   
    
      <li><a href="../the-pencil-tool/index.html">The Pencil Tool</a></li>
    
   
    
      <li><a href="../the-pen-tool/index.html">The Pen Tool</a></li>
    
   
    
      <li><a href="index.html">The Calligraphy Tool</a></li>
    
   
    
      <li><a href="../boolean-operations/index.html">Boolean Operations</a></li>
    
   
    
      <li><a href="../about-nodes/index.html">About Nodes</a></li>
    
   
    
      <li><a href="../editing-paths/index.html">Editing Paths</a></li>
    
   
    
      <li><a href="../stacking-order/index.html">Stacking Order</a></li>
    
   
    
      <li><a href="../align-and-distribute/index.html">Align and Distribute</a></li>
    
   
    
      <li><a href="../the-node-tool/index.html">The Node Tool</a></li>
    
   
    
      <li><a href="../tracing-an-image/index.html">Tracing an Image</a></li>
    
   
    
      <li><a href="../the-tweak-tool/index.html">The Tweak Tool</a></li>
    
   
    
      <li><b>Color</b></li>
    
   
    
      <li><a href="../colors/index.html">Colors</a></li>
    
   
    
      <li><a href="../the-palette/index.html">The Color Palette</a></li>
    
   
    
      <li><a href="../the-fill-and-stroke-dialog/index.html">The Fill And Stroke Dialog</a></li>
    
   
    
      <li><a href="../custom-colors/index.html">Custom Colors</a></li>
    
   
    
      <li><a href="../copying-a-color/index.html">Copying a Color</a></li>
    
   
    
      <li><a href="../creating-gradients/index.html">Creating Gradients</a></li>
    
   
    
      <li><a href="../creating-a-mesh-gradient/index.html">Creating a Mesh Gradient</a></li>
    
   
    
      <li><a href="../using-a-pattern/index.html">Using Patterns</a></li>
    
   
    
      <li><a href="../modifying-patterns/index.html">Modifying Patterns</a></li>
    
   
    
      <li><a href="../creating-custom-patterns/index.html">Creating Custom Patterns</a></li>
    
   
    
      <li><a href="../strokes/index.html">Strokes</a></li>
    
   
    
      <li><a href="../markers/index.html">Markers</a></li>
    
   
    
      <li><a href="../ordering-markers-stroke-and-fill/index.html">Ordering Markers, Stroke and Fill</a></li>
    
   
    
      <li><a href="../customizing-markers/index.html">Customizing Markers</a></li>
    
   
    
      <li><b>Text</b></li>
    
   
    
      <li><a href="../text/index.html">Text</a></li>
    
   
    
      <li><a href="../writing-text/index.html">Writing Text</a></li>
    
   
    
      <li><a href="../styling-text/index.html">Styling Text</a></li>
    
   
    
      <li><a href="../changing-text-color/index.html">Changing Text Color</a></li>
    
   
    
      <li><a href="../moving-and-rotating-letters/index.html">Moving and Rotating Letters</a></li>
    
   
    
      <li><a href="../putting-text-on-a-path/index.html">Putting Text on a Path</a></li>
    
   
    
      <li><a href="../flowing-text-into-a-frame/index.html">Flowing Text into a Frame</a></li>
    
   
    
      <li><a href="../creating-fonts/index.html">Creating Custom Fonts</a></li>
    
   
    
      <li><b>Advanced Techniques</b></li>
    
   
    
      <li><a href="../advanced-techniques/index.html">Beyond the Basics</a></li>
    
   
    
      <li><a href="../saving-your-work/index.html">Saving Your Work</a></li>
    
   
    
      <li><a href="../exporting-a-pdf-file/index.html">Exporting a PDF File</a></li>
    
   
    
      <li><a href="../exporting-a-png-file/index.html">Exporting a PNG File</a></li>
    
   
    
      <li><a href="../exporting-to-other-file-formats/index.html">Exporting to other File Formats</a></li>
    
   
    
      <li><a href="../importing-pictures-into-a-document/index.html">Importing Pictures into a Document</a></li>
    
   
    
      <li><a href="../importing-other-file-formats/index.html">Importing other File Formats</a></li>
    
   
    
      <li><a href="../copying-and-duplicating-objects/index.html">Copying and Duplicating Objects</a></li>
    
   
    
      <li><a href="../cloning-objects/index.html">Cloning Objects</a></li>
    
   
    
      <li><a href="../customizing-keyboard-shortcuts/index.html">Customizing Keyboard Shortcuts</a></li>
    
   
    
      <li><a href="../clipping-and-masking/index.html">Clipping and Masking</a></li>
    
   
    
      <li><a href="../filters/index.html">Filters</a></li>
    
   
    
      <li><a href="../extensions/index.html">Extensions</a></li>
    
   
    
      <li><a href="../live-path-effects/index.html">Live Path Effects</a></li>
    
   
    
      <li><a href="../where-to-go-next/index.html">Where to Go from Here</a></li>
    
   
    
      <li><a href="../test-chapter-for-trying-out-styling/index.html">Test Chapter (for trying out styling)</a></li>
    
