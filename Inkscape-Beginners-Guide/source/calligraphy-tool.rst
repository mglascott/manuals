The Calligraphy Tool
====================

What was a goose quill in antiquity, is Inkscape's calligraphy tool in
the digital world. Ideally, it should be used with a graphics tablet and
stylus, with one hand on the graphics tablet and the other one on the
keyboard, which moves the canvas, so one can write without interruption.

|Icon for Calligraphy Tool|  [Ctrl]+[F6] or [c]

 

|image1|

 

A varied set of options exists for simulating different brushes, in case
the default brushes that come with Inkscape do not suffice.

-  **Dip pen**. Emulates a bevelled pen tip.
-  **Marker**. Emulates a round and regular tip.
-  **Brush**. Emulates a soft, thinning ellipse.
-  **Wiggly**. Emulates a very jumpy round brush.
-  **Splotchy**. Emulates a quill.
-  **Tracing**. This allows you to emulate an engraving, by drawing more
   or less regular lines over a drawing that serves as a model.

The button to the right, **Add or edit calligraphic profile**, allows
you to save and load the settings made in the following options, under a
brush name of your choice.

Every brush is a result of the following parameters:

-  **Width**. Here you can set the width of your brush.
-  The **Pressure sensitivity** icon is only useful when a graphics
   table is used for drawing.
-  The **Trace lightness icon** needs to be active when you want to use
   the Engraving feature.
-  **Thinning**. Determines the width of the brush stroke at the start
   and end of the line.
-  **Angle**. Determines the quill's angle.
-  **Fixation**. Determines how much the angle will change when the draw
   direction changes.
-  **Caps**. Determines the shape of the brush stroke's ends.
-  **Tremor**. Adds a little random to the brush stroke.
-  **Wiggle**. Adds the unexpected.
-  **Mass**. Simulates the weight of the tool used, which has an impact
   on the stroke's shape.

|Dip Pen setting of Calligraphy Tool|

The ends of a line drawn with the Dip Pen setting are cusp.

|Marker setting of Calligraphy Tool|

With the Marker setting, the paths look smooth and their ends are
rounded.

|Brush setting of Calligraphy Tool|

The setting Brush creates slightly rounded and uneven strokes.

|Wiggly setting of Calligraphy Tool|

The Wiggly setting allows you to draw very organic shapes.

|Splotchy setting of Calligraphy Tool|

When set to Splotchy, the result looks a bit like calligraphy.

|Tracing setting of Calligraphy Tool|

With the calligraphy tool setting Tracing, drawings that look like an
old engraving can be created. The image below serves as a model.

.. |Icon for Calligraphy Tool| image:: images/calligraphy_tool_icon.png
   :width: 50px
   :height: 50px
.. |image1| image:: images/callygraphy_menu.png
   :width: 100.0%
.. |Dip Pen setting of Calligraphy Tool| image:: images/calligraphy_tool_dip_pen.png
   :width: 516px
.. |Marker setting of Calligraphy Tool| image:: images/calligraphy_tool_marker.png
   :width: 494px
   :height: 477px
.. |Brush setting of Calligraphy Tool| image:: images/calligraphy_tool_brush.png
   :width: 529px
.. |Wiggly setting of Calligraphy Tool| image:: images/calligraphy_tool_wiggly.png
   :width: 589px
   :height: 566px
.. |Splotchy setting of Calligraphy Tool| image:: images/calligraphy_tool_splotchy.png
   :width: 542px
   :height: 459px
.. |Tracing setting of Calligraphy Tool| image:: images/calligraphy_tool_tracing.png

