Text
====

There is one menu which is dedicated to everything related to **Text**, which makes it easier to add text to adverts or logos. The text can be added as a normal text, or in a frame for a float text. At any time, the font, font size and color of the text can be changed. You can also make interesting modifications such as making a text follow the course of a path or flowing it into an irregularly-shaped frame. Once a text has been converted to a path, the text can be deformed just like any other path, to achieve a specific visual design or typography.

Typographers will appreciate the option to create SVG fonts with Inkscape. The dedicated dialog allows you to directly see the result, in a preview that updates as you type!

.. toctree::

   writing-text
   styling-text
   changing-text-color
   moving-and-rotating-letters
   putting-text-on-path
   flowing-text-into-frame
   creating-custom-fonts
