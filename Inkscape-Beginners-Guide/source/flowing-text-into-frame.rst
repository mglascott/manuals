Flowing Text into a Frame
=========================

Whether you're creating a graphical poem (calligram), or whether your
text needs a specific shape to fit into your layout, Inkscape offers you
a tool to assist with this type of design.

|image0|

To use it:

-  1. Write the text on the canvas (or copy-paste it).
-  2. Create the path or shape that the text is supposed to fill.
-  3. With the Selector tool, select the shape and the text, then use
   Text > Flow into Frame.

The path stays editable, the text will try to fill it as good as
possible. The text, too, can still be edited any time.

Very often, this is just the first step in arranging a text in a
decorative fashion. Some finishing touches may be needed to improve the
positioning of certain words or letters, by using the options offered by
the Text tool.

To further tweak the text, there is one last, irreversible option: the
transformation of a text into a path. When you convert a text into a
path, you will lose any possibility of editing the text's contents. The
text will no longer be available as a text object, but it now allows you
to use all the available options for editing paths. For example, it will
become impossible to change the font, the font weight (light, bold,
heavy,...) or the style (italic, regular, condensed,...). Because of
this, it is recommended to work on the rough and general shape of a text
first, and to later move on to the finer details:

-  Finish everything that needs to be done with the Text tool, to
   benefit from the tool's property of keeping the text itself editable.
-  Only convert the text to a path at the very end, to be able to tweak
   the small details of the text.

If you would like to share the SVG file on the web, or to export it into
a PDF file, that you want to share with other people, convert all text
objects to paths. Not all computers have the same fonts installed, and
the file may look very different to someone, if their computer uses a
fallback font to display the text.

 

|image1|

Text flowed into a frame that resembles an octopus.

.. |image0| image:: images/icon-texte-encadre.png
   :width: 30px
   :height: 30px
.. |image1| image:: images/illu-texte6.png

