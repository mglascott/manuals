Filters
=======

One of the menu entries is dedicated to **filters**. More precisely, all
of these are combinations of SVG filters. The possibilities of
combination are infinite, and the Inkscape developers put some
combination suggestions into the menu and tried to find a descriptive
name for each.

To apply a filter, you need to first select one or more objects, then
select the filter you want to use. The result will immediately be
available on the canvas.

|image0|

The Drop Shadow Filter
----------------------

This filter promises to automatically add a shadow to the selected
object that will be placed below the object. First, select one or more
objects, then open the filter's dialog with Filters > Shadows and Glows>
Drop Shadow... This dialog allows you to define the amount of blur, the
shadow's offset, it's type, and its color. The larger the blur radius,
the more distant will the object seem from its shadow. The farther away
the values for the x and y offset are from 0, the more will it look as
if the light comes from the side. The dropdown for the shadow type
allows you to choose between Outer (normal shadow), Inner (the object
looks like it were a hole), Outer Cutout (just the Outer shadow, the
object will be invisible), Inner Cutout (just the Inner shadow, without
the object) or Shadow Only (the full shadow, without the object). In the
Color tab, you can change the shadows color and opacity (the intensity
of the shadow). If you want to play with different values, check the
little box labelled **Live Preview**, then the result will be shown on
the canvas. It will only be applied to the object when you click on
**Apply**.

|image1|

Editing Filters
---------------

Inkscape offers a dialog for editing filters, which can be opened from
the entry Filter Editor... at the bottom of the Filters menu.

When the dialog opens, and there is no filter applied to the currently
selected objects, it will be empty, else you will see the components of
the object's current filter. The basic principle of the dialog is
simple, but understanding all its ramifications is highly complex. One
needs to have a deep knowledge in SVG filters and matrix mathematics to
be able to understand how to use the paramaters of every available
filter to achieve a specific result. Because of this, the easier option
may be to just play with the filter settings randomly, starting from one
of the stock filters that is similar to the desired result.

Here is a list of interesting filters:

-  Distort > Chalk and Sponge: turns objects into an interesting
   explosion.
-  Image Paint and Draw > Pencil: converts the selected objects to a
   sketch.
-  Scatter: Several filters that give different scatter effects.
-  Blurs: Offers several blur variations.
-  Morphology: Offers filters that have to do with the objects'
   contours.

It's up to you to explore this further. The result of most filters
depends heavily on the selected objects.

.. |image0| image:: images/menu-filtre.png
.. |image1| image:: images/ombre-portee-w.png

