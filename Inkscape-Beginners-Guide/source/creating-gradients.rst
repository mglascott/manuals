Creating Gradients
==================

Inkscape allows you to comfortably create and modify gradients
on-canvas, but it is useful to always have the Fill and Stroke dialog
within reach, to be able to modify the colors easily.

|image0|\ [Ctrl]+[F1] or [g]

The **Gradient Tool** can be activated in the tool bar. First, you need
to select an object that you want to have a gradient, then
click-and-drag over the object with the Gradient tool. There will now
appear two new handles on the object, one square, one circular, which
are connected by a blue line. The square symbolizes the beginning of the
gradient, and the circle its end. By default, the tool creates linear
gradients with two stops (i.e. 2 handles with one color each).

To change a gradient's position, just move the square or the circular
handle with the mouse. To edit its colors:

-  select one handle of the gradient (circular or square) with the
   Gradient tool
-  in the Fill and Stroke dialog, select a color, which will then be
   applied to the object, at the location of the handle, or click on a
   color in the palette to assign it to the selected gradient stop.

To create a gradient that has more than 2 colors, you can double-click
with the Gradient tool on the blue line that spans between the handles.
This will create a diamond-shaped handle. You can now change its color.
This will immediately show on the canvas. When you move the
diamond-shaped handle along the line with the mouse, the colors of the
gradient get different weights.

You can transform a linear gradient into a radial one by using the top
row buttons in the Fill and Stroke dialog, or you can replace the old
gradient by creating a new gradient with the gradient tool, with the
option for radial gradients selected.

When an object has a **radial gradient** applied to it, the gradient is
symbolized not by a straight line, but by an angle, that has a square
handle in the middle, and two circular handles at the end of its arms.
The length of the arms represents the diameter of the radial gradient.
Working with it is very similar to working with linear gradients.

There can be a separate gradient applied to an object's fill and to its
stroke.

|image1|

A rectangle that has a gradient that goes from yellow to transparent. At
the top, the tool controls bar indicates that the object has a linear
gradient with two stops, one yellow, the other transparent. Each stop
can be modified with the Fill and Stroke dialog, after the corresponding
stop has been selected on the canvas.

|image2|

A linear gradient with 5 stops.

|image3|

 

The position of each stop (color) can be changed directly on the canvas.

|image4|

A linear gradient can be transformed into a radial gradient and the
other way around.

|image5|

A radial gradient can be moved as a whole by dragging on the square
handle.

.. |image0| image:: images/gradient_tool_icon.png
   :width: 50px
   :height: 50px
.. |image1| image:: images/gradient_tool_linear.png
.. |image2| image:: images/gradient_tool_linear_fill_stops.png
.. |image3| image:: images/gradient_tool_linear_fill_move_stops.png
.. |image4| image:: images/gradient_tool_circular_fill.png
.. |image5| image:: images/gradient_tool_circular_fill_move.png

