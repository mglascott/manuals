Align and Distribute
====================

|Icon for Align and Distribute dialog| [Ctrl + Shift + A]

This dialog offers invaluable help. You can use it to align nodes and
objects vertically or horizontally, or to distribute them at equal
distances.

To use it for aligning nodes:

-  1. Open the dialog by clicking on its icon in the command bar to the
   right of the canvas.
-  2. Switch to Node tool.
-  3. Select a couple of nodes.
-  4. Click on the node alignment button of your choice. The alignment
   will be applied immediately.

For aligning objects:

-  1. Open the dialog
-  2. Switch to Select tool.
-  3. Select the objects that you want to align.
-  4. Decide which one of the objects should not move at all. The object
   you selected first? The one you selected last? The biggest or the
   smallest one? Or perhaps you want to move all the objects in relation
   to the page? In the dropdown labelled Relative to:, select the option
   that applies.
-  5. Click on the alignment button of your choice. Use the tooltips
   that display when you hover over them with the mouse to learn what
   each symbol means. The selected alignment will be applied
   immediately.

|image1|

Before arranging the objects, they are distributed somewhat randomly on
the canvas.

|image2|

After aligning each row and column vertically and horizontally, the
result looks very neat.

 

|The Align and Distribute dialog in object mode|

With the Align and Distribute dialog, it only takes a couple of clicks
to arrange your objects neatly.

.. |Icon for Align and Distribute dialog| image:: images/align_distribute_icon.png
   :width: 30px
   :height: 30px
.. |image1| image:: images/desordre.png
   :width: 449px
.. |image2| image:: images/ordre.png
   :width: 509px
   :height: 490px
.. |The Align and Distribute dialog in object mode| image:: images/align_distribute_dialog_win10.png
   :width: 266px
   :height: 380px
