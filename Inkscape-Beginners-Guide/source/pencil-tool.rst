The Pencil Tool
===============

|Icon for Pencil Tool| 

Enable the Pencil (or sometimes called Freehand tool) by clicking the
icon above, or by pressing [F6] or [p].

The behavior of the pencil tool depends on the settings in its controls
bar. To draw with this tool, press the left mouse button and drag the
mouse around the canvas. The pencil will leave a green trace that
follows the location of the mouse cursor. When you let go of the mouse
button, the shape you created will get its stroke (and/or its fill, if
you have one set).

Two tiny, square handles appear at the start and end of the drawn path.
When you start drawing on one of these handles, this will continue the
path, instead of creating a new object.  And if you stop drawing the
same path in one of those squares, it will close the path (meaning there
are no openings).

Let's have a look at the options of the pencil tool. The Shape dropdown
menu offers different brushes that influence the shape of the path:

-  **Triangle in**, **out**. This makes the path look a little more
   elegant, thinning or thickening along its length. Switch to the node
   tool and drag on the pink diamond-shaped handle to adjust the width
   interactively.
-  **Ellipse**. The beginning and end of the path will be thinner than
   its middle part.
   Switch to the node tool and drag on the round white handle to adjust
   the width interactively.
-  **From clipboard**. You can make custom brushes by first drawing your
   brush shape, and then copying it. This will add it to the clipboard
   automatically and can be used as a brush for the pencil and pen
   tools.
-  **Bend from clipboard**.  First you must copy a curve that already
   exists. The line that you draw will be deformed like the path that
   you copied.  And it will be adjustable, using the Node tool.
-  **Last applied**. This is simil
-  **None**. The drawn path's outline will be of the same width along
   the whole length of the path.

You can set the amount of **smoothing** for the path you want to draw.

|Icons for LPE-based smoothing|

The button LPE based interactive simplify allows you to draw a path
where you can adjust the smoothing after you have finished drawing the
path. The button LPE simplify flatten will lock the result of the
interactive smoothing. After it has been used, the path's smoothing can
no longer be adjusted.

*Additional info: LPE is the acronym for "Live Path Effect", a set of
functionalities specific to Inkscape that can change paths
non-destructively, to achieve spectacular results.*

To reset your changes to smoothing to the default value, you can use the
button where the hover text says 'reset pencil parameters to default'.

The pencil tool has three different **modes**. The results you get also
depend a lot on the level of smoothing:

-  **Create regular Bézier path**. The path that you get as a result is
   very close to the path that you drag with the mouse cursor on the
   canvas. The more smoothing you apply, the more artistic and elegant
   (and less scrawly) the result will be.
-  **Create Spiro path**. Create stylish swirls and curls with only the
   mouse!
-  **Create BSpline path**. This mode reveals its use when you switch to
   the node tool. It makes it really easy to draw evenly smooth curves.

The pencil tool (as well as the pen tool) creates **dots** when you hold
down the Ctrl key while clicking on the canvas. When you hold down both
Ctrl and Shift, the dots' size will be doubled. With Ctrl and Alt,
random sized small dots will be created with every click, and with Ctrl,
Alt and Shift, each click will generate a random sized big dot.

|Dots created with the pencil tool|

Dots created with the pencil tool. Top left: with Ctrl, top right: with
Ctrl + Alt, bottom left: with Ctrl + Shift, bottom right: with Ctrl +
Alt + Shift. Note that the dots are really circles.

|A path drawn with Shape: Ellipse and no smoothing|

A path drawn with Shape: Ellipse and no smoothing.

| The path has been extended from the square handle at its end.|

 The path has been extended from the square handle at its end.

|Shape: Ellipse with more smoothing|

Shape: Ellipse with more smoothing.

|Shape: Triangle Out was used here.|

Shape: Triangle out was used here.

|Path with Shape: From Clipboard|

This path uses Shape: From Clipboard

|This path was copied to the clipboard.|

The path that was copied to the clipboard for drawing the above path.

|Path with Shape: Triangle In, Bézier mode, smoothing set to 40|

Path drawn with Shape: Triangle In in Bézier mode, with a smoothing of
40

|Path drawn in B-Spline mode|

Path drawn in B-Spline mode

|Path drawn in Spiro mode with Shape: From clipboard|

Path with Shape: From Clipboard drawn in Spiro mode with a smoothing
level of 40

|The width of the path can be changed with the white circular handle.|

The same path as above, only wider. The width of a path that uses
objects on the clipboard (and those that use Shape: Ellipse, both use
the Pattern along Path Live Path Effect) can be changed with the Node
tool, using the white circular handle at the beginning of the path.

|The width of the path can be changed with the pink diamond-shaped
handle.|

The width of a path with a PowerStroke Live Path Effect (used by Shape:
Triangle In / Out) can be adjusted with the node tool by moving a pink,
diamond-shaped node, to achieve a non-constant path width.

.. |Icon for Pencil Tool| image:: images/pencil_tool_icon.png
   :width: 50px
   :height: 50px
.. |Icons for LPE-based smoothing| image:: images/lpe_based_simplification.png
   :width: 122px
   :height: 66px
.. |Dots created with the pencil tool| image:: images/pencil_tool_dots.png
   :width: 430px
   :height: 361px
.. |A path drawn with Shape: Ellipse and no smoothing| image:: images/pencil_tool_ellipse_nosmooth.png
   :width: 309px
   :height: 134px
.. | The path has been extended from the square handle at its end.| image:: images/pencil_tool_ellipse_nosmooth_extended.png
.. |Shape: Ellipse with more smoothing| image:: images/pencil_tool_ellipse_smoothed.png
.. |Shape: Triangle Out was used here.| image:: images/pencil_tool_triangle_out.png
   :width: 469px
   :height: 103px
.. |Path with Shape: From Clipboard| image:: images/pencil_tool_shape_from_clipboard.png
   :width: 392px
   :height: 155px
.. |This path was copied to the clipboard.| image:: images/pencil_tool_shape_for_clipboard.png
   :width: 166px
   :height: 86px
.. |Path with Shape: Triangle In, Bézier mode, smoothing set to 40| image:: images/pencil_tool_triangle_in_smoothing_bezier.png
   :width: 347px
   :height: 125px
.. |Path drawn in B-Spline mode| image:: images/pencil_tool_b-spline_smoothing.png
   :width: 317px
   :height: 118px
.. |Path drawn in Spiro mode with Shape: From clipboard| image:: images/pencil_tool_spiro_from_clipboard.png
   :width: 340px
   :height: 184px
.. |The width of the path can be changed with the white circular handle.| image:: images/pencil_tool_spiro_from_clipboard_path.png
   :width: 340px
   :height: 195px
.. |The width of the path can be changed with the pink diamond-shaped handle.| image:: images/pencil_tool_triangle_out_path.png
   :width: 340px
   :height: 155px
