Importing other File Formats
============================

Inkscape can import and open a large variety of file formats. The availabilty of some import file formats depends on other programs installed on your computer. Third-party Inkscape extensions can be used to import additional file formats.

When you import certain file formats, Inkscape may open a dialog asking you to specify import options for the file.

For some file formats, Inkscape is only able to import a subset of all available features of that file format.

Commonly available import file formats
--------------------------------------

(ordered by file extension)

- **Adobe Illustrator (via pdftocairo) (\*.ai)** - Adobe Illustrator file import via pdftocairo**
- **Adobe Illustrator 8.0 and below (\*.ai)** - Adobe Illustrator files created with Illustrator 8.0 or older
- **Adobe Illustrator 9.0 and above (\*.ai)** - Adobe Illustrator files created with Illustrator 9.0 and newer
- **Adobe Illustrator SVG (\*.ai.svg)** - SVG file exported from Adobe Illustrator
- **ani (\*.ani)** - file format for animated mouse cursors on MS Windows
- **wmf (\*.apm)** - same as Windows Metafiles (\*.wmf)
- **bmp (\*.bmp)** - Windows Bitmap image file format
- **Corel DRAW Compressed Exchange files (\*.ccx)** - Corel DRAW exchange file format
- **Corel DRAW Compressed Exchange files (UC) (\*.ccx)** - Corel DRAW exchange file format
- **Corel DRAW 7-X4 files (\*.cdr)** - Corel DRAW file format version 7-X4
- **Corel DRAW 7-X4 files (UQ) (\*.cdr)** - Corel DRAW file format version 7-X4
- **Corel DRAW 7-13 template files (\*.cdt)** - Corel DRAW template file format version 7-13
- **Corel DRAW 7-13 template files (UC) (\*.cdt)** - Corel DRAW template file format version 7-13
- **Computer Graphics Metafile files (\*.cgm)** - standardized graphics format for vector and raster data
- **Corel DRAW Presentation Exchange files (\*.cmx)** - Corel DRAW exchange file format for presentations
- **Corel DRAW Presentation Exchange files (UC) (\*.cmx)** - Corel DRAW exchange file format for presentations
- **ico (\*.cur)** - non-animated cursors file format for MS Windows
- **ACECAD Digimemo File (\*.dhw)** - ACE CAD digital notepad notes file
- **Dia Diagram (\*.dia)** - Diagram file format from Dia
- **AutoCAD DXF R13 (\*.dxf)** - dxf CAD file format from AutoCAD, version 13
- **Enhanced Metafiles (\*.emf)** - proprietary Windows vector file format that only supports straight lines
- **Encapsulated PostScript (\*.eps)** - embeddable printer graphics file
- **GdkPixdata (\*.gdkp)** - pixel data files for GTK
- **gif (\*.gif)** - graphics interchange format, a common raster file format (animations not supported in Inkscape)
- **GIMP Gradient (\*.ggr)** - gradient file from Gimp, gradient will be available in the Fill and Stroke dialog
- **HP Graphics Language file (\*.hpgl)** - plotter file format
- **icns (\*.icns)** - Apple icon image file format
- **ico (\*.ico)** - Windows computer icon file format
- **jpeg2000 (\*.j2k)** - see \*.jp2
- **jpeg2000 (\*.jp2)** - standardized compressed raster image file format
- **jpeg2000 (\*.jpc)** - see \*.jp2
- **jpeg (\*.jpe)** - see \*.jpeg
- **jpeg (\*.jpeg)** - Joint Photographic Experts Group compressed raster image file format
- **jpeg (\*.jpg)** - see \*.jpeg
- **jpeg2000 (\*.jpf)** - see \*.jp2
- **jpeg2000 (\*.jpx)** - extended jpg2000 file format, see jp2
- **pnm (\*.pbm)** - portable bitmap file format (black and white only)
- **pcx (\*.pcx)** - ZSoft picture exchange raster graphics file format
- **pnm (\*.pgm)** - portable graymap file format (grayscale)
- **Adobe PDF (\*.pdf)** - standardized document exchange file format
- **Adobe PDF (via pdftocairo) (\*.pdf)** - standardized document exchange file format, import via pdftocairo library
- **HP Graphics Language Plot file [AutoCAD] (\*.plt)** - plotter file format
- **png (\*.png)** - portable network graphics, a lossless raster image file format
- **pnm (\*.pnm)** - portable anymap file format (see also pbm, pgm and ppm)
- **pnm (\*.ppm)** - portable pixmap file format (RGB)
- **PostScript (\*.ps)** - printer graphics language file format
- **qtif (\*.qif)** - see \*.qtif
- **qtif (\*.qtif)** - Quick Time image file format
- **ras (\*.ras)** - Sun (Solaris) raster file format
- **sK1 vector graphics files (\*.sk1)** - native file format of sk1 vector editor
- **Scalable Vector Graphic (\*.svg)** - SVG files, such as the files Inkscape generates
- **Compressed Inkscape SVG (\*.svgz)** - gzip-compressed SVG file with all editor data
- **tga (\*.targa)** - see \*.tga
- **tga (\*.tga)** - Truevision Advanced Raster Graphics Array image file
- **tiff (\*.tif)** - see \*.tiff
- **tiff (\*.tiff)** - Tagged Image File Format (raster file format)
- **Microsoft Visio 2013 drawing (\*.vsdm)** - drawings created in MS Visio 2013
- **Microsoft Visio 2013 drawing (\*.vsdx)** - drawings created in MS Visio 2013
- **Microsoft Visio XML Diagram (\*.vdx)** - diagrams created in MS Visio
- **Microsoft Visio Diagram (\*.vsd)** - diagrams created in MS Visio
- **wbmp (\*.wbmp)** -&nbsp; Wireless Application Protocol Bitmap Format (black-transparent raster graphics)
- **Windows Metafiles (\*.wmf)** - improved version of .wmf with support for B&eacute;zier curves
- **WordPerfect Graphics (\*.wpg)** - graphics exported from Word Perfect
- **Microsoft XAML (\*.xaml)** - Microsoft's graphical user interface description language file format
- **xbm (\*.xbm)** - X BitMap file format for cursors (black and white)
- **xpm (\*.xpm)** - X PixMap file format for cursors (color, successor of \*.xbm)
