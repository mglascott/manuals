Creating Custom Patterns
========================

You can make your own patterns in Inkscape. A pattern can consist of a
texture (i.e. a raster image like \*.jpg or \*.png that you imported), a
group, a path, shape objects,...

Select the object that you would like use as a pattern, and then tell
the software to turn it into a pattern via Object > Pattern > Objects to
Pattern. The option below this allows you to do the opposite operation.
This is useful, if you want to slightly modify your pattern. Your
pattern will appear on the canvas, applied to a rectangle, and it will
also be available next to the stock patterns that come with Inkscape.

Your first patterns may look a little surprising to you. When you start
designing beautiful seamless patterns, your are engaging in the art of
illustration.

|image0|

Custom pattern

.. |image0| image:: images/pattern05.png
   :width: 743px
