Boolean Operations
==================

There are multiple ways to modify an object in your drawing. If the
object is a rectangle, an ellipse, a star or a spiral, i.e. a
geometrical shape drawn with one of the shape tools, then this object is
a shape, and not a path. Shapes can only be modified by dragging their
specific handles. In contrast to shapes, you must use tools that are
meant for modifying paths, if you want to edit a path. **Shapes can be
converted into paths**, so the path tools can then be used with them.

To convert a shape into a path:

-  1. Select the shape with theSelector tool.
-  2. In the menu, selectPath > Object to path.

Warning: a shape can always be converted to a path, but a path can never
be converted back into an object!

The Boolean Operations work on paths, or they try to convert the
selected objects to paths before they compute the result. All these
operations need at least 2 convertible objects or paths, which will be
combined following specific rules.

-  **|image0| Union**. Keeps the common outline of all selected paths.
-  **|image1|** **Difference**. Subtracts one path from another one.
-  **|image2| Intersection**. Only keeps those parts that are covered by
   all selected paths.
-  **|image3| Exclusion**. Keeps those parts which are covered by an
   uneven number of paths (if you have two objects, this is where the
   objects do not overlap).
-  **|image4| Division**. The path below is cut into pieces by the path
   above.
-  **|image5| Cut Path**. Creates as many paths as there are path
   intersections between the two paths.
-  **|image6| Combine**. Keeps all parts, and combines them into a
   single object.
-  **|image7| Break Apart**. If a path consists of a number of
   independent parts (subpaths), this will create that number of
   separate objects.

To use these operations, select the two (or more) objects, and then
select the option of your choice in the Path menu. The result will
immediately appear on the canvas - if it doesn't, read the error message
that will appear in the status bar, at the bottom of the Inkscape
window, to find out about the reason for the failure.

|Two objects about to be unioned|

|The result of unioning the triangle and the square|

Unioning a triangle and a square gives a house.

|The rectangle will become the door opening|

|The door is open|

 Difference between a rectangle and a house creates an opening for the
door.

|Two overlapping ellipses|

Two overlapping ellipses.

|Intersection between the two ellipses|

Intersection between the two ellipses.

|Exclusion between the two ellipses|

Exclusion between the two ellipses.

|Ellipse with a path on top|

Someone has drawn a path with the pencil tool (with the setting of
shape: Ellipse) on the orange ellipse.

|Ellipse divided by the path|

Division.

|The two parts of the ellipse have been combined into a single path
(with subpaths)|

Move apart and combine (to form a single path composed of two subpaths).

|Each subpath has become a single path after Break Apart.|

Break Apart separates all subpaths into independent objects.

.. |image0| image:: images/boolops_union_icon.png
   :width: 40px
   :height: 40px
.. |image1| image:: images/boolops_difference_icon.png
   :width: 40px
   :height: 40px
.. |image2| image:: images/boolops_intersection_icon.png
   :width: 40px
   :height: 41px
.. |image3| image:: images/boolops_exclusion_icon.png
   :width: 40px
   :height: 40px
.. |image4| image:: images/boolops_division_icon.png
   :width: 40px
   :height: 40px
.. |image5| image:: images/boolops_cut_path_icon.png
   :width: 40px
   :height: 40px
.. |image6| image:: images/boolops_combine_icon.png
   :width: 40px
   :height: 40px
.. |image7| image:: images/boolops_break_apart_icon.png
   :width: 40px
   :height: 41px
.. |Two objects about to be unioned| image:: images/boolops_union_1.png
   :width: 435px
   :height: 432px
.. |The result of unioning the triangle and the square| image:: images/boolops_union_2.png
   :width: 264px
   :height: 262px
.. |The rectangle will become the door opening| image:: images/boolops_difference_1.png
   :width: 351px
   :height: 348px
.. |The door is open| image:: images/boolops_difference_2.png
   :width: 355px
   :height: 354px
.. |Two overlapping ellipses| image:: images/boolops_intersection_1.png
   :width: 339px
   :height: 248px
.. |Intersection between the two ellipses| image:: images/boolops_intersection_2.png
   :width: 64px
   :height: 181px
.. |Exclusion between the two ellipses| image:: images/boolops_exclusion.png
   :width: 349px
   :height: 346px
.. |Ellipse with a path on top| image:: images/boolops_division_1.png
   :width: 314px
   :height: 468px
.. |Ellipse divided by the path| image:: images/boolops_division_2.png
   :width: 297px
   :height: 475px
.. |The two parts of the ellipse have been combined into a single path (with subpaths)| image:: images/boolops_combine.png
   :width: 233px
   :height: 395px
.. |Each subpath has become a single path after Break Apart.| image:: images/boolops_break_apart.png
   :width: 239px
   :height: 392px
