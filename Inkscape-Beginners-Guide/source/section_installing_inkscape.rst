Installing Inkscape
===================

.. toctree::

   installing-on-windows
   installing-on-mac
   installing-on-linux
