Free Drawing
============

The freehand drawing tools make it possible to draw directly onto the Inkscape canvas using the mouse or a graphics tablet stylus. Depending on what and how you would like to draw, you can select the best tool for the task. These tools are not based on geometrical shapes. You can draw exactly the shape you need. With some practice, you will get better and better in achieving exactly the result that you desire. And of course, you can always modify the elements in your drawing with the path tools.

In this section, we're going to first learn about how to use the **Pencil, Pen and Calligraphy tools**. This way, we will gently learn about the new concept of **paths**, which are at the core of vector drawings. Later on, we will explore how to edit these paths.

.. toctree::

   pencil-tool
   pen-tool
   calligraphy-tool
   boolean-operations
   about-nodes
   editing-paths
   stacking-order
   align-and-distribute
   node-tool
   tracing-an-image
   tweak-tool
