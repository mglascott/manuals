Copying a color
===============

There are several ways to apply the same style to multiple objects:

-  **Selecting multiple objects**: select all the objects whose color
   you want to change (holding the Shift key or dragging a selection box
   around them), then open the Fill and Stroke dialog, select a color.
   It will be applied to all selected objects automatically.
-  **Use the Dropper tool**: select an object, then activate the Dropper
   tool by clicking on its icon, then click on the canvas, on an area
   where it has the color that you want to apply. The selected object
   will change its color accordingly.
-  |image0|\ **Copy the complete style of an object to apply it to
   another object:** select the object, copy it with Ctrl + C, then
   select the second object and paste only the style with Ctrl + Shift +
   V. This not only copies the fill and stroke color, but also the
   stroke width and style, a text's font style and any patterns or
   gradients.

.. |image0| image:: images/dropper_tool_icon.png
   :width: 50px
   :height: 50px
