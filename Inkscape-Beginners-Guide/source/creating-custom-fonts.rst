Creating Custom Fonts
=====================

With HTML5 and CSS3, it has become easy to use custom fonts on your web
site. Fonts can be saved in different file formats: OTF, TTF and SVG.
The letters, numbers and special characters a font consists of are
called glyphs.

Inkscape provides a dialog dedicated to the creation of SVG fonts in the
menu under Text > SVG Font Editor. There is a special template prepared
for you to use when you want to create a font, which is meant for
drawing one glyph per layer. When you create a font, this basically
means that you will need to create as many glyphs as your font is
supposed to contain. To open the template, use File > New from Template
> Typography Canvas. For creating an SVG font, follow these steps:

-  1. In the column labelled Font, click on New to create a font. You
   can double-click on the generic name of the font to change it.
-  2. In the Glyphs tab, click on Add Glyph to indicate that you want to
   create a new glyph. Double-click on the Glyph name field to name your
   glyph, and on the Matching String field to enter the letter that it
   corresponds to.
-  3. Draw the path for your glyph on the canvas.
-  4. When you're happy with your glyph, select it, and also select the
   corresponding row in the dialog, then click on Get curves from
   selection.

Repeat this process for all the glyphs in your font. You can always test
your font by typing a text into the field at the bottom of the SVG Font
Editor dialog and looking at the preview above it.

This functionality is meant for typographers, but amateurs, too, can
quickly get a working result and test their work as they go.

When your font is finished, you can use a software like
`FontForge <../../../../../../external.html?link=https://fontforge.github.io/>`__
where you import your SVG font and can export it into different formats
to be able to use it with other software.

If you speak French, read the book "`Fontes
Libres <../../../../../../external.html?link=https://fr.flossmanuals.net/fontes-libres/>`__"
to learn more about creating typography.

|image0|

Template for creating typography

Dialog for creating SVG fonts

.. |image0| image:: images/illu-texte7.png

