Changing Text Color
===================

You can change a text's color the same way as you change the color of
any vector object in Inkscape. First, you need to select the text, or,
using the Text tool, select words or characters in a text, then you
click on the color of your choice. The text can have a separate paint
for fill and stroke, like all the objects in Inkscape. You can also
apply a gradient to a text.

|image0|

Text variations

.. |image0| image:: images/illu-texte3.png

