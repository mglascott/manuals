Advanced Techniques
===================

Now you have familiarized yourself with the basics of the Inkscape program. While Inkscape may appear complex when you first use it, with some practice, you will soon feel more confident.

In this chapter, you will discover functionality that can be used to automate or simplify many steps. However, don't forget that originality comes from your imagination, and not from a software that autogenerates the results for you.

First, we will take a look at how to save your work for being able to reuse it, then we will learn about some useful techniques, and we will finish with presenting some filters, extensions and path effects.

.. toctree::

   saving
   export-pdf
   export-png
   export-other-formats
   import-pictures
   import-other-formats
   copy-and-duplicate
   cloning-objects
   custom-keyboard-shortcuts
   clipping-and-masking
   filters
   extensions
   live-path-effects
