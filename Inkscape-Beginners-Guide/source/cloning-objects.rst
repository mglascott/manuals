Cloning Objects
===============

Very often, people could save time if only they used clones more
frequently. With Alt + d, a clone can be created just as quickly as a
copy or a duplicate. One only needs to remember that the option exists.
The clone will be put on top of the original, and will follow all
modifications made on the original, no matter if they affect its style
(stroke, fill,...) or its shape. The clone's shape cannot be edited, and
its colors can only be changed if the original has them 'unset' (the
question mark button in the Fill and Stroke dialog). To turn a clone
into an independent, fully editable object, you need to **unlink** it
from its original with Edit > Clone > Unlink Clone.

|image0|

.. |image0| image:: images/illu-clone01-w.png

