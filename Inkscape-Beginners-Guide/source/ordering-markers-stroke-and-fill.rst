Ordering Markers, Stroke and Fill
=================================

An object's path determines its shape. The stroke is centered on the
path, one half of it on the inside of the path, overlapping with the
fill, the other half on the outside. Markers, too, are centered on the
path.

In the Order section of the Fill and Stroke dialog, you can select the
order in which these different parts of an object will be drawn. This
way, you can place markers (and / or strokes) above or below the fill.

 

|image0|

 

Example image needed!

 

[top] markers > stroke > fill [bottom]

|image1|

[top] markers > fill > stroke [bottom]

|image2|

[top] stroke > marker > fill [bottom]

|image3|

[top] stroke > fill > markers [bottom]

|image4|

[top] fill > markers > stroke [bottom]

|image5|

[top] stroke > fill > marker [bottom]

|image6|

.. |image0| image:: images/ordre-marqueurs.png
   :width: 275px
   :height: 175px
.. |image1| image:: images/order_markers_example_1.png
   :width: 709px
   :height: 400px
.. |image2| image:: images/order_markers_example_2.png
   :width: 712px
   :height: 401px
.. |image3| image:: images/order_markers_example_3.png
   :width: 713px
   :height: 400px
.. |image4| image:: images/order_markers_example_4.png
   :width: 711px
   :height: 400px
.. |image5| image:: images/order_markers_example_5.png
   :width: 711px
   :height: 398px
.. |image6| image:: images/order_markers_example_6.png
   :width: 713px
   :height: 403px
