Creating a Mesh Gradient
========================

This tool is especially useful to those who want to create
photorealistic designs, but it also has its uses for everyone who wants
to create complex gradients in a single object.

The Mesh Gradient tool can be activated in the tool bar |image0|, or you
can add a mesh gradient to an object from the Fill and Stroke dialog
when you select the **Mesh Gradient** mode |image1|.

 

First, select the object that you would like to add a mesh gradient to.
Then, either click-and-drag on it with the Mesh Gradient tool, or click
on the icon in the  Fill and Stroke dialog. The mesh gradient will be
displayed directly on the object. It consists of different kinds of
nodes:

-  grey diamond shaped nodes that you can assign a color to
-  white circular (or arrow-shaped) handles which help to shape the
   curve of the mesh.

In the tool's tool controls bar, note the fields labelled Rows and
Columns. More rows or columns add more nodes that can each have a
separate color. This way, an object can be painted in a multitude of
colors. New meshes will have the set number of rows or columns.

To add more rows or columns to an existing gradient, double-click on the
vertical or horizontal mesh lines.

To apply a color: 

-  select a grey node
-  select the color of your choice.

You can use the dropper icon at the bottom of the Fill and Stroke dialog
to apply colors that you already use in your drawing more quickly.

Just like the normal gradients, **mesh gradients** can be shared between
objects, when both objects use a gradient with the same name (e.g.
mygradient1234). Select the gradient in the Mesh Fill dropdown in
the Fill and Stroke dialog to reuse it on a different object.

|image2|

This cloud consists of 10 different colors. Each color has been applied
to one of the grey diamond-shaped nodes. The shape of the mesh has been
modified to better fit the shape of the cloud.

 

|image3|

With mesh gradients.

 

|image4|

The same landscape without mesh gradients.

.. |image0| image:: images/mesh_gradient_tool_icon.png
   :width: 40px
   :height: 40px
.. |image1| image:: images/paint_gradient_mesh_icon.png
   :width: 40px
.. |image2| image:: images/mesh_gradient_cloud.png
.. |image3| image:: images/mesh_gradient_landscape_with_mesh.png
.. |image4| image:: images/mesh_gradient_landscape_without_mesh.png

