Strokes
=======

An object's stroke is handled independently from its fill. Two of the
tabs of theFill and Stroke dialog are dedicated to styling strokes.

You can assign a paint to a stroke, this works in the same way as
assigning a paint to the fill does, using one of the available modes
(only in the Stroke tab). The stroke paint can not only be a flat color,
but also a radial or linear gradient, a pattern or even a gradient mesh.

[[reminder: fix wording in other according places, so that color/style
-> paint, translated incorrectly before! - in French it's always
'color'.]]

In the Stroke Style tab, you can set the width of the stroke, its joins
and end caps or select a dash pattern (simple stroke, various dash
lengths,...).

It is impossible to apply blur only to an object's stroke. The options
for Blur and Opacity always apply to the object as a whole.

To give an object's stroke a width, 1 px is usually enough to be
visible, but not excessive. For those who are used to printing terms, 1
px corresponds to the smallest possible stroke [[in French: filet, no
idea about English - I'd say 1 dot, but that only has a single
dimension, and we need two - can someone research this, please?]] for
images that are meant for displaying on a computer screen. A dropdown
menu with units is useful for those who are not familiar with pixels.

Check the zoom level (in %) to be sure that you're applying a suitable
width. The more you zoom in, the wider the stroke will appear.

The options for Joins and Caps slightly modify the behavior of the
stroke. The default values are fully sufficient in most cases.

The option labelles Dashes gives access to an important drop-down menu
with a large selection of different dash styles, that give you regular
dash patterns along the stroke. The number field to the right allows you
to shift the dash pattern to the desired location.

|image0|

Black flat color on stroke

Linear gradient with 3 stops. The dotted lines indicate the spread of
the color for each stop.

Radial gradient with 5 stops on the stroke. The dotted ellipses indicate
the spread of the color for each stop.

The 3 different types of line joins.

.. |image0| image:: images/illu-contour1.png

