Test Chapter (for trying out styling)
=====================================

This chapter is meant for trying out how to best style the book.

This is a keyboard shortcut: Ctrl + A

This is a special note, with a hint for advanced users.

|This is an image with a description.|

This is an image with a description.

This is a list:

-  Item 1
-  item 2
-  Item 3

 

 

This is a description for where to click in the user interface: In the
menu, select File → Save as... (or File > Save as...), then click on OK.
Open the Align and Distribute dialog.

--------------

Test keyboard shortcuts formatting
----------------------------------

Css for keys
~~~~~~~~~~~~

`` key{ ``

``border:1px solid gray;``

`` font-size:1.2em; ``

``box-shadow:1px 0 1px 0 #eee, 0 2px 0 2px #ccc, 0 2px 0 3px #444;``

`` -webkit-border-radius:3px;``

`` -moz-border-radius:3px;``

`` border-radius:3px;``

`` margin:2px 3px;``

`` padding:1px 5px;``

`` }``

this looks like:

|
| Ctrl

 

Embedded in the text - no good!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tellus
enim, porttitor eget sodales id, porttitor consectetur lectus. Cras
condimentum leo non accumsan semper. Maecenas commodo viverra euismod.
Phasellus iaculis mi a magna porta pharetra. Sed semper aliquet arcu vel
fermentum. Vivamus non rutrum risus, nec ornare diam. Nunc vitae neque
in tellus molestie tincidunt. Proin sit amet est luctus, Ctrl suscipit
lectus non, commodo mauris. Vestibulum elementum aliquet lectus, nec
sollicitudin purus varius a. Etiam quis nulla vitae mi condimentum
rhoncus. Curabitur mollis augue felis, vitae tincidunt erat fringilla
in. Fusce tincidunt commodo nibh. Donec ullamcorper non nisi molestie
auctor. Maecenas sollicitudin orci at eleifend fermentum. Nunc ut dui
posuere, ultrices turpis ac, facilisis odio. Praesent vitae placerat
odio. Suspendisse condimentum neque a dui aliquet aliquam. Ut consequat
nisl mollis, laoreet tortor lacinia, porta orci. Nulla placerat nec
turpis sit amet molestie. Ut in ullamcorper urna. Donec rhoncus dui
fringilla massa fringilla auctor. Quisque quis porta leo. In sit amet
purus eu augue commodo cursus ac eget sem. In pulvinar augue quis
rhoncus aliquam. Vestibulum bibendum volutpat tortor, et efficitur purus
egestas a. Fusce sit amet luctus dui. Ut magna libero, suscipit sed
sagittis iaculis, bibendum vel mauris. Aenean justo quam, bibendum et
euismod vel, viverra ut ligula. Vestibulum fringilla felis mi, at
ullamcorper lacus varius a. Etiam mattis pharetra dapibus. Sed sagittis
mauris eu facilisis sollicitudin.

One liner - not too bad
~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur Ctrl adipiscing elit. Duis
tellus enim, porttitor eget sodales id, porttitor consectetur lectus.

Test smaller font - Fit in a paragraph
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tellus
enim, porttitor eget sodales id, porttitor consectetur lectus. Cras
condimentum leo non accumsan semper. Maecenas commodo viverra euismod.
Phasellus iaculis mi a magna porta pharetra. Sed semper aliquet arcu vel
fermentum. Vivamus non rutrum risus, nec ornare diam. Nunc vitae neque
in tellus molestie tincidunt. Proin sit amet est luctus, suscipit lectus
non, commodo mauris. Vestibulum elementum aliquet lectus, nec
sollicitudin purus varius a. Etiam quis nulla vitae mi condimentum
rhoncus. Curabitur mollis augue felis, vitae tincidunt erat fringilla
in. Fusce tincidunt commodo nibh. Donec ullamcorper non nisi molestie
auctor. Maecenas sollicitudin orci at eleifend fermentum. Nunc ut dui
posuere,ultrices turpis ac, facilisis odio. Praesent vitae placerat
odio. Suspendisse condimentum neque a dui aliquet aliquam. Ut consequat
nisl mollis, laoreet tortor lacinia, porta orci. Nulla placerat nec
turpis ``Ctrl`` sit amet molestie. Ut in ullamcorper urna. Donec rhoncus
dui fringilla massa fringilla auctor. Quisque quis porta leo. In sit
amet purus eu augue commodo cursus ac eget sem. In pulvinar augue quis
rhoncus aliquam. Vestibulum bibendum volutpat tortor, et efficitur purus
egestas a. Fusce sit amet luctus dui. Ut magna libero, suscipit sed
sagittis iaculis, bibendum vel mauris. Aenean justo quam, bibendum et
euismod vel, viverra ut ligula. Vestibulum fringilla felis mi, at
ullamcorper lacus varius a. Etiam mattis pharetra dapibus. Sed sagittis
mauris eu facilisis sollicitudin.

 

--------------


Short-cuts:
-----------

The pen tool:

 

| Icon for Pen Tool|\  [Shift]+[F6] or [b]

 

The calligraphy tool

|Icon for Calligraphy Tool|\   [Ctrl]+ [F6] or [c]

 

.. |This is an image with a description.| image:: images/boolops_difference_1.png
   :width: 351px
   :height: 348px
.. | Icon for Pen Tool| image:: ../the-pen-tool/static/pen_tool_icon.png
   :width: 50px
   :height: 50px
.. |Icon for Calligraphy Tool| image:: images/calligraphy_tool_icon.png
   :width: 50px
   :height: 50px
