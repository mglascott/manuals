The Tweak Tool
==============

This is the ideal tool for illustrators who first draw their designs on
paper, then scan and vectorize their work. It allows you to modify paths
on the canvas by making use of its numerous options, without ever
switching tools.

|Icon for Tweak Tool| [Shift]+[F2] or [w]

To use it, **select** the path or the part of the path that you would
like to modify with the **Node tool**. Then switch to the **Tweak tool**
and **click** on the area that you wish to edit.

First, let's have a look at its parameters:

-  **Width**. Determines the size of the tool. If you only want to edit
   a small region on the canvas, make the circle smaller, else, make it
   bigger.
-  **Force**. Determines how strong the effect of the tool will be.
-  |Icon for activating pressure sensitivity|\ The **Pressure
   sensivity** icon should be activated when your graphics tablet has
   support for pressure. However, the tool can be used well without a
   graphics tablet.

Now let's look at the heart of the tool, its different modes:

-  |Icon for pushing objects with Tweak Tool| **|Icon for attracting
   objects with Tweak Tool| |Icon for moving objects randomly with Tweak
   Tool| Move objects**. The first three icons move the selected objects
   in various directions. Read the hint in the status bar to learn about
   additional key presses that change how the tool works
-  **|Icon for shrinking objects with Tweak Tool| Shrink/Grow objects**.
   Reduces the size of the paths, or, when you hold down the Shift key,
   enlarges the paths.
-  **|Icon for rotating objects with Tweak Tool| Rotate**. Rotates the
   paths. 
-  **|Icon for duplicating objects with Tweak Tool| Duplicate**. Creates
   duplicates of the selected paths. These will be placed directly above
   the originals, so remember to move them, if you want to see the
   effect.
-  **|Icon for pushing path parts with Tweak Tool| Push path parts**.
   Deforms the paths by pushing the nodes like a snow shovel.
-  **|Icon for shrinking parts of paths with Tweak Tool| Shrink/**
   **|Icon for growing parts of paths with Tweak Tool|** **Grow path
   parts**. The borders of a part of a path will be moved closer to each
   other, with Shift, they will be moved farther away from each other.
-  **|Icon for roughening paths with Tweak Tool| Roughen path parts**.
   Roughens the contours of a path.
-  **|Icon for painting objects with Tweak Tool| Paint color**
   and \ |Icon for randomizing colors of objects with Tweak Tool|
   **Jitter colors** modify the color of the paths.
-  **|Icon for blurring objects with Tweak Tool| Blur objects**. Adds
   blur to the paths.

 

|Chameleon image to be tweaked|

This drawing will serve as an example. The chameleon was drawn with a
pencil, and afterwards it has been vectorized with Inkscape.

|Chameleon tail editing with tweak tool|

The tail needs to be edited, so it will gradually become thinner. Here,
the effect has been exaggerated for demonstration purposes.

|The chameleon's tongue is being roughened with Tweak Tool| Some
roughening has been added to the tongue, to emphasize the beast's
dangerous side.

.. |Icon for Tweak Tool| image:: images/tweak_tool_icon.png
   :width: 40px
   :height: 40px
.. |Icon for activating pressure sensitivity| image:: images/pressure_sensitivity_icon.png
   :width: 30px
   :height: 30px
.. |Icon for pushing objects with Tweak Tool| image:: images/tweak_tool_push_icon.png
   :width: 30px
   :height: 30px
.. |Icon for attracting objects with Tweak Tool| image:: images/tweak_tool_attract_icon.png
   :width: 30px
   :height: 31px
.. |Icon for moving objects randomly with Tweak Tool| image:: images/tweak_tool_randomize_icon.png
   :width: 30px
   :height: 30px
.. |Icon for shrinking objects with Tweak Tool| image:: images/tweak_tool_shrink_icon.png
   :width: 30px
   :height: 31px
.. |Icon for rotating objects with Tweak Tool| image:: images/tweak_tool_rotate_icon.png
   :width: 30px
   :height: 33px
.. |Icon for duplicating objects with Tweak Tool| image:: images/tweak_tool_duplicate_icon.png
   :width: 30px
   :height: 30px
.. |Icon for pushing path parts with Tweak Tool| image:: images/tweak_tool_push_path_icon.png
   :width: 30px
   :height: 30px
.. |Icon for shrinking parts of paths with Tweak Tool| image:: images/tweak_tool_shrink_path_icon.png
   :width: 30px
   :height: 30px
.. |Icon for growing parts of paths with Tweak Tool| image:: images/tweak_tool_grow_path_icon.png
   :width: 30px
   :height: 30px
.. |Icon for roughening paths with Tweak Tool| image:: images/tweak_tool_roughen_path_icon.png
   :width: 30px
   :height: 30px
.. |Icon for painting objects with Tweak Tool| image:: images/tweak_tool_paint_icon.png
   :width: 30px
   :height: 30px
.. |Icon for randomizing colors of objects with Tweak Tool| image:: images/tweak_tool_jitter_color_icon.png
   :width: 30px
   :height: 30px
.. |Icon for blurring objects with Tweak Tool| image:: images/tweak_tool_blur_icon.png
   :width: 30px
   :height: 30px
.. |Chameleon image to be tweaked| image:: images/tweak_tool_chameleon_orig.png
.. |Chameleon tail editing with tweak tool| image:: images/tweak_tool_chameleon_shrink_tail.png
.. |The chameleon's tongue is being roughened with Tweak Tool| image:: images/tweak_tool_chameleon_roughen_tongue.png

