The Selector Tool
=================

|image0|\ This tool is symbolized by an arrow. It is the top button in
the toolbar, and is also accessible with either F1 or S key. This is a
fundamental tool in the program, since almost everything must be
selected before it can be edited.

Working much like a hand, the Select tool also moves, scales, skews and
rotates objects.

To move an object:

-  position the mouse over an object;
-  press the mouse button and hold while dragging it to the desired
   position (hold the Ctrl key to move the shape in horizontal or
   vertical direction only);
-  release the mouse button.

|image1|

*Drag to move the selection. Transformations (such as moving, scaling,
rotating) are easy thanks to the two-way arrows. Click the selection a
second time to access skew and rotating functions.*

To scale (resize) an object:

-  click on it to select it;
-  position the mouse over a two-way arrow on a side or a corner;
-  press the mouse button and hold while dragging it to the desired size
   (also hold the Ctrl key to preserve the proportions);
-  release the mouse button.

To skew an object:

-  select it, then click it again;
-  position the mouse over a horizontal or vertical two-way arrow;
-  press the mouse button and hold while dragging it to the desired
   amount of skew (also hold the Ctrl key for 15° increments)
-  release the mouse button.

To rotate an object:

-  select it, then click it again;
-  grab a curved two-way arrow in any corner;
-  drag it until the object reaches the desired angle (also hold the
   Ctrl key for 15° increments).

|image2|

In some cases, you want to edit more than one object at once. So the
Select tool can select more than one object at once.

To select more than one object, there are mainly two ways:

-  click the first object, then hold the Shift key while clicking each
   additional object once;
-  or, press the mouse button and drag out a rectangular selection box
   which encloses all the objects.

|image3|

*Each selected object is framed with a dashed line (known as the
bounding box), while the two-way arrows for transformation are placed
around the entire selection (one or more than one object).*

You can combine the two methods: hold the Shift key to keep previously
selected objects selected, and drag out a selection box to add more
objects to the selection. Or vice versa works too. After selecting more
than one object by dragging a selection box, hold the Shift key while
you individually click on more objects.

Also notice how the Shift key allows you to sort of toggle a selection:
click to add to the selection, click again to remove.

.. |image0| image:: images/selector-tool.png
   :width: 50px
   :height: 50px
.. |image1| image:: images/select-tool-move-scale.png
.. |image2| image:: images/select-tool-skew-rotate.png
.. |image3| image:: images/select-tool-selection-frame.png

