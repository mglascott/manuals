Squares and Rectangles
======================

|Rectangle Tool's icon|

The Rectangle tool can be enabled by clicking the icon shown above, or
by pressing either the F4 or R key. For drawing a rectangle, click and
drag the mouse diagonally, using the same motion as when dragging a
selection box. The rectangle will appear immediately after you release
the mouse button. To draw a perfect square, hold down the Ctrl key while
click-dragging with the mouse.

The square-shaped handles can be used to change the rectangle's size.
However, when you only want to change either the height or the width of
the rectangle, it's hard to control with the mouse alone. Here you can
use the Ctrl key to restrict the size change to either width or height
of the rectangle.

|image1|

*When you hold down Ctrl while dragging the square-shaped handles, it is
easy to limit the change in the rectangle's size to a single direction.*

The circle-shaped handles are for rounding the corners. Grab a circle
handle and move it just a tiny bit. You'll see that all four corners
will be rounded. Additionally, a second circle handle has appeared. Each
handle changes the amount of rounding in one direction (It is not
possible to only round a single corner of the rectangle independant from
the others. They all change together).

|The rectangle's circular handles|

*Moving the circle handles rounds the corners. Each circle handle
changes the radii in a different direction.*

*
*

|The rectangle's corner radii|

*The "Rx" and "Ry" values on the control bar, determine the radius of
the imaginary circle which the rounding is based upon.*

To restore the initial, sharp-cornered shape of a rectangle or square,
click on the far right icon in the tool control bar (which is a tiny
version of the image below). This is very useful, when you're still
learning how to master the usage of the circular handles!

|Rectangle Tool: make corners sharp icon|

When you need to draw a rectangle with accurate dimensions, you can use
the tool control bar:

-  the field labelled "W" is for the width;
-  the field labelled "H" is for the height;
-  the "Rx" and "Ry" fields define the rounding radius for the corners.

(For rectangles and squares, this can also be done from the Select tool
control bar.)

The dropdown menu allows you to select the unit you need for your
rectangle.

|Rectangle Tool's tool controls bar|

.. |Rectangle Tool's icon| image:: images/rectangle_tool.png
   :width: 50px
   :height: 50px
.. |image1| image:: images/rectangle_square_handles_1.png
   :width: 415px
   :height: 194px
.. |The rectangle's circular handles| image:: images/rectangle_circular_handles.png
   :width: 444px
   :height: 147px
.. |The rectangle's corner radii| image:: images/rectangle_corner_radii.png
   :width: 444px
   :height: 145px
.. |Rectangle Tool: make corners sharp icon| image:: images/rectangle_make_corners_sharp_icon.png
   :width: 50px
   :height: 50px
.. |Rectangle Tool's tool controls bar| image:: images/rect-tool-controls-bar_win10.png
   :width: 482px
   :height: 169px
