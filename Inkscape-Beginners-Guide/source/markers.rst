Markers
=======

Markers are little symbols that can be added to the start, along the
middle and to the end of a path. They will be placed on the path's
stroke. This is very useful when you want to create arrows and diagrams
quickly!

Markers will be located exactly on the nodes of the path. The start
marker will appear on the first node of the path, the middle markers on
each node along the path, and the end marker on the last node of the
path.

Inkscape offers lots of different markers for you to choose from. You
can look at them in the Stroke Style tab of the Fill and Stroke dialog,
in the row labelled Markers. The left-most drop-down menu is for the
start marker, the one in the middle for the middle markers, and the
right-most one for the end markers.

Markers automatically take on the fill and stroke of the path they are
attached to. In older Inkscape versions, the extension Extensions >
Modify Path > Color Markers... will do this for you.

| If your arrows point into the wrong direction, you can change the
  direction of the path (Path > Reverse).
| |image0|

List of available markers

.. |image0| image:: images/illu-marqueur1.png

