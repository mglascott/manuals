Exporting a PDF File
====================

The PDF file format is a vector format that can also hold raster images. As Inkscape is a vector editor, it can save directly to pdf by saving a copy as PDF (never save as PDF only, but always keep an SVG file, because the PDF file format supports a different set of features than the SVG file format and you may lose data in the process).

*Note that for creating perfect pdf files for printing, it's better to turn to a dedicated desktop publishing software such as `Scribus <https://www.scribus.net/>`_, which can also import SVG files.*

Convert your texts to paths to be sure that your PDF files will look the same across all devices.

*Inkscape can also open PDF files for editing.*
