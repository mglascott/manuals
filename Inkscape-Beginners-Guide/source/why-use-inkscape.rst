Why use Inkscape?
=================

There are many image creation and manipulation software programs.
However there are two main types of images:

-  **Raster** or **bitmap** images such as images from digital cameras.
   They are made up of pixels, and the quality is reduced the more the
   images are manipulated. Zooming or shrinking can cause blurriness or
   pixelation.
-  **Vector** images are created with vector graphics software. They are
   made up of mathematical paths, and are resolution independent – the
   image adapts to the available space without losing quality. This is
   why the same vector image can be used for different sizes of final
   image presentation. For example, the same image can be used for a
   business card or a large poster with equal quality.

Depending on how the image is used, either raster or vector graphics may
suit better to your needs.

|image0|

.. |image0| image:: images/vector-format.png
   :width: 300px
