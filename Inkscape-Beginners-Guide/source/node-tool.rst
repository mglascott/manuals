The Node Tool
=============

This tool offers a number of options we haven't seen yet. In this
chapter we will go through them, by looking at the different icons in
its tool controls bar:

|Icon for Node Tool| [F2] or [n]

 

**|Icon for inserting nodes| Insert new node**. A double-click on a path
segment lets you add new nodes easily directly on the canvas. If you
need to add more nodes, or want to insert a node right in the middle
between two other nodes, you can click on the path segment or select
multiple nodes, then use the button. More nodes allow you to more
precisely modify a path.

**|Icon for deleting nodes| Delete selected nodes**. Select the node(s)
and then either use the [Del] key, or use the button. It helps to try to
have as few nodes as possible, because this allows you to make changes
to the objects in your drawing more quickly.

|Icon for merging nodes| To **join (merge) nodes**, you need to select
at least two nodes. Inkscape will try to preserve the path's shape.

**|Icon for splitting nodes| Break path**. This will split one node into
two nodes. These two new nodes are not connected by a path segment. The
new nodes only have a single handle, as they are end nodes, and they are
placed directly on top of each other. This can sometimes be difficult to
handle. Only use this feature when you really need it!

**|Icon for joining nodes with a segment| Join end nodes with a new
segment**. This is ideal for manually joining separate path pieces.

**|Icon for deleting a segment between two nodes| Delete segment between
two non-end nodes**. Breaks a path in two.

 

The next couple of icons can be used to **convert** one thing into a
different one:

-  the **different node types into each other**, which we already talked
   about in the `chapter 'About nodes' <../about-nodes/index.html>`__;
-  |Icon for converting objects to paths| **objects to paths**, as we
   have learnt in the `chapter about Boolean
   operations <../boolean-operations/index.html>`__;
-  |Icon for converting strokes to paths| **strokes of objects to
   separate objects**;

Then, there are number fields for changing the x and y coordinate of the
selected node, and a drop-down menu that allows you to change the unit
for the coordinates.

 

|image9| |image10| |image11| |image12|  |image13| |image14|

These buttons determine if certain path properties will be editable and
visible on the canvas.

 |A node has been added by double-clicking.|

An object where a node has been added by double-clicking on the path.

|The node that looks smaller is actually two nodes stacked.|

A stroke was added, and another node. This node was broken into two.
This one looks a lot smaller. Make sure you really want this before
clicking the button!

|A segment has been deleted.|

 

The segment between two nodes has been deleted.

|These nodes can now be edited.|

Example of editing a path by ...

|A node was moved.|

... moving a node, then ...

|Node type and segment shape have been changed.|

... changing a node's type and modifying the handles.

.. |Icon for Node Tool| image:: images/node_tool_icon.png
   :width: 50px
   :height: 50px
.. |Icon for inserting nodes| image:: images/node_tool_insert_node_icon.png
   :width: 40px
   :height: 40px
.. |Icon for deleting nodes| image:: images/node_tool_delete_node_icon.png
   :width: 40px
   :height: 40px
.. |Icon for merging nodes| image:: images/node_tool_merge_nodes_icon.png
   :width: 40px
   :height: 40px
.. |Icon for splitting nodes| image:: images/node_tool_split_nodes_icon.png
   :width: 40px
   :height: 40px
.. |Icon for joining nodes with a segment| image:: images/node_tool_connect_nodes_icon.png
   :width: 40px
.. |Icon for deleting a segment between two nodes| image:: images/node_tool_delete_segment_icon.png
   :width: 40px
   :height: 40px
.. |Icon for converting objects to paths| image:: images/object_to_path_icon.png
   :width: 40px
   :height: 40px
.. |Icon for converting strokes to paths| image:: images/stroke_to_path_icon.png
   :width: 40px
   :height: 40px
.. |image9| image:: images/node_tool_show_clip_path_icon.png
   :width: 40px
   :height: 40px
.. |image10| image:: images/node_tool_show_mask_icon.png
   :width: 40px
   :height: 40px
.. |image11| image:: images/node_tool_next_path_effect_parameter_icon.png
   :width: 40px
   :height: 40px
.. |image12| image:: images/node_tool_transform_handles_icon.png
   :width: 40px
   :height: 40px
.. |image13| image:: images/node_tool_show_node_handles_icon.png
   :width: 40px
   :height: 40px
.. |image14| image:: images/node_tool_show_path_outline_icon.png
   :width: 40px
   :height: 40px
.. |A node has been added by double-clicking.| image:: images/node_tool_node_added_example.png
   :width: 628px
.. |The node that looks smaller is actually two nodes stacked.| image:: images/node_tool_split_node_example.png
.. |A segment has been deleted.| image:: images/node_tool_delete_segment_example.png
.. |These nodes can now be edited.| image:: images/node_tool_node_editing_1_example.png
.. |A node was moved.| image:: images/node_tool_node_editing_2_example.png
.. |Node type and segment shape have been changed.| image:: images/node_tool_node_editing_3_example.png

