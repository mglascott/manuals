About Nodes
===========

It's the nature of all paths to be made up of nodes. Each path consists
of at least two nodes. A node's position is marked by a square, circle
or diamond shape. The two circular handles (or control points) of a node
are connected to it by thin lines, and can be grabbed with the mouse.
The tools that work on paths all create or modify path nodes.

The second-most used tool in Inkscape is the **Node tool**. It allows
you to edit all paths.

There are multiple node types in Inkscape:

 

-  |Cusp Node Icon| **Cusp nodes**. Used for creating corners, or for
   being able to freely modify the curvature of the path. The handles
   can be moved independantly.
-  |Smooth node icon| **Smooth nodes**. Used for drawing beautiful,
   flowing curves. Both handles and the node are aligned on a straight
   line.
-  |Symmetric node icon| **Symmetric nodes**. Used for drawing soft
   curves. The handles are not only on the same line, but also both at
   the same distance from the node.
-  |Auto-smooth node icon| **Auto-smooth nodes**. Used for drawing nice
   curves, without worrying about handles or segment shapes. The handles
   sit on a straight line, and their distance from the node adapts
   automatically when you move the node, so a smooth curve is drawn.

To edit a node:

-  1. Switch to the **Node tool** by clicking on its icon.
-  2. Click on the path you want to modify.
-  3. Click on the node that you would like to convert to a different
   node type, or select multiple nodes.
-  4. Then click on the corresponding icon in the tool controls bar to
   set the node type.

Beginners often prefer to use cusp nodes, because they are easy to use,
although very often, smooth nodes would be the better choice.

The next chapter will explain how to modify a path with the Node tool.

|Cusp nodes make sharp corners.|

Cusp nodes make sharp corners.

|A smooth node creates a rounded curve.|

A smooth node creates a rounded curve.

|A symmetric node creates a symmetrical curve.|

A symmetric node creates a symmetrical curve.

|Auto-smooth nodes adapt automatically when you move them.|

Auto-smooth nodes adapt automatically when you move them.

.. |Cusp Node Icon| image:: images/node-type-cusp.png
   :width: 40px
.. |Smooth node icon| image:: images/node-type-smooth.png
   :width: 40px
.. |Symmetric node icon| image:: images/node-type-symmetric.png
   :width: 40px
.. |Auto-smooth node icon| image:: images/node-type-auto-smooth.png
   :width: 40px
.. |Cusp nodes make sharp corners.| image:: images/nodes_cusp_example.png
   :width: 407px
   :height: 461px
.. |A smooth node creates a rounded curve.| image:: images/nodes_smooth_example.png
   :width: 445px
   :height: 366px
.. |A symmetric node creates a symmetrical curve.| image:: images/nodes_symmetric_example.png
   :width: 455px
   :height: 373px
.. |Auto-smooth nodes adapt automatically when you move them.| image:: images/nodes_auto-smooth_example.png
   :width: 515px
   :height: 366px
