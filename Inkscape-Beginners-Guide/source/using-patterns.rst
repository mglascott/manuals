Using Patterns
==============

Inkscape comes with a couple of preset patterns. To apply them to an
object, you need to:

-  select the object on the canvas
-  open the Fill and Stroke dialog, and in the Fill (or Stroke) tab,
   click on the icon for patterns |image0|

| You will then see a dropdown menu with many pattern names. The pattern
  you select will immediately be applied to the object.

Selecting a pattern from the list applies it directly to the object.

|image1|

Object with stock pattern 'Polka dots, medium' and a view of the list of
available patterns

.. |image0| image:: images/paint_pattern_icon.png
   :width: 30px
   :height: 30px
.. |image1| image:: images/pattern_polka_dots_medium_with_pattern_list.png

