Editing Paths
=============

The **Node tool** will be your friend when you need to **edit a path**.

|Icon for Node Tool| [F2] or [n]

No matter the node type, each node can only be connected to two other
nodes - one that **comes before**, and one that **comes after**. This
means that it's impossible to attach a third line to a node. Instead,
one has to create another path.

A path's shape can be changed radically by moving the nodes it consists
of:

-  Activate the Node tool.
-  Click on the path to select it.
-  Click and drag the node you wish to reposition.

For more gradual changes, paths can be edited by moving their nodes'
handles:

-  Activate the Node tool.
-  Click on the path to select it.
-  Click on the node you wish to edit.
-  Click and drag on the node's handle(s).

You can also manipulate the segments of a path, i.e. the lines between
two nodes, directly with the Node tool. Click and drag on the path
segment that connects two nodes, to change its curvature, without having
to use the handles for this.

 

|The selected node has an incoming and an outgoing path segment.|

 This node has an incoming and an outgoing path segment connected to it.

|You can move a node by grabbing it with the mouse.|

You can move a node by grabbing it with the mouse.

 

|You can modify path segments by moving the node's handles.|

You can modify path segments by moving the node's handles.

.. |Icon for Node Tool| image:: images/node_tool_icon.png
   :width: 50px
   :height: 50px
.. |The selected node has an incoming and an outgoing path segment.| image:: images/node_tool_node_with_adjacent_segments.png
.. |You can move a node by grabbing it with the mouse.| image:: images/node_tool_move_node.png
.. |You can modify path segments by moving the node's handles.| image:: images/node_tool_shaping_segments.png

