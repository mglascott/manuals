The Pen Tool
============

When you're first using this tool, it may appear complex, but very soon
it will become a good friend that you will enjoy to use for creating
your illustrations.

| Icon for Pen Tool| [Shift]+[F6] or [b]

This tool's purpose is to draw paths. Multiple paths, drawn next to and
on top of each other, form a design. For a start:

-  1. Click with the left mouse button to create a the first point
   (node) of the path.
-  2. Move the mouse and click again. Repeat this for as many nodes that
   you want to draw. All your nodes will be connected by straight lines
   (segments).
-  3. To finish the path, right-click with the mouse.

It's also possible to draw curves with this tool. Every node you create
has two handles. These handles define the shape of the curve. To draw a
curve directly, you can:

-  1. Click with the left mouse button to position the first node.
-  2. Keep the mouse button pressed and move the mouse slightly.
-  3. You are currently moving one handle of the node.
-  4. Left-click and drag to continue the path, or right-click to finish
   it.

The first method is predestined for creating precise drawings. First,
you position the nodes, later, you can use the node tool to modify the
path.

You'll recognize the modes (Bézier, Spiro and BSpline) and the Shape
option from the Pencil tool. There is also one mode for drawing only
straight lines, and one that only allows you to draw horizontal or
vertical lines.

With some experience, you will learn how to place your nodes
strategically. When a path contains too many nodes, this will make
working with it more difficult, and when there aren't enough of them,
you will find that you have a lack of control when you are tweaking the
path.

 

|Using the Pen Tool|

Every left-click adds a node. A long left-click drags out a node's
handle and makes the segment curvy.

|Creating the rough shape|

Creating the rough shape with the Pen tool, without thinking too much
about any curves. The nodes are represented as grey diamonds.

|Nodes have been aligned|

The nodes have been aligned vertically and horizontally using the
Align-and-Distribute dialog.

|Segments have been curved|

The path has been modified by making the straight segments curved with
the help of the node tool.

|Some ninor adjustments have been made|

Some final adjustments have been made, to achieve a nice result. The
image now resembles an open book.

.. | Icon for Pen Tool| image:: images/pen_tool_icon.png
   :width: 50px
   :height: 50px
.. |Using the Pen Tool| image:: images/pen_tool_usage.png
   :width: 600px
   :height: 529px
.. |Creating the rough shape| image:: images/pen_tool_draw_book_1.png
   :width: 359px
   :height: 272px
.. |Nodes have been aligned| image:: images/pen_tool_draw_book_2.png
   :width: 349px
   :height: 262px
.. |Segments have been curved| image:: images/pen_tool_draw_book_3.png
   :width: 348px
   :height: 262px
.. |Some ninor adjustments have been made| image:: images/pen_tool_draw_book_4.png
   :width: 333px
   :height: 227px
