Saving Your Work
================

Inkscape is a stable software. However, some unfortunate actions can entrail a crash. The best way to prevent this from causing problems is to regularly save your drawing. For this reason, the keyboard shortcut :kbd:`Ctrl` + :kbd:`s` should be used frequently. When you first save your drawing, give your file a name of your choice, and select one of these extensions:

- .svg (Inkscape SVG): to keep all the data in your file, even the data that is only useful for editing with Inkscape, and no other editor
- .svg (Plain SVG): for sharing your drawing with other people, who might not use Inkscape, but use SVG, e.g. when they view a web page that uses a picture that you created
- .svgz (compressed Inkscape / Plain SVG): compressed to save space on your harddisk

Activating Autosave prevents loss of work
-----------------------------------------

If Inkscape closes unexpectedly, in most cases it creates a backup file that is available in the location that will be indicated in a short dialog window. The file name consists of the original file name with the time and date of the crash. It is recommended that (additionally to saving frequently) you activate Inkscape's autosave feature, which you can find in Edit &gt; Preferences &gt; Input/Output &gt; Autosave.
