Color
=====

Colors can be added and changed at any time when you're working with Inkscape. For a vector object, a separate style can be defined for its outline (or **stroke**) and its main color (or **fill**). This can be a **plain color **(e.g. dark red), a **pattern** (e.g. stripes or flowers), or a **gradient** (e.g. a smooth transition from green to blue).

Inkscape comes with a couple of patterns, but also makes it easy to create your own **custom patterns**. Adding a plain color - that is, a uniform color - can easily be achieved by using one of the ready-made palettes. Gradients are created directly on the canvas with the dedicated tool.

In this section, we are going to learn how to apply a plain color in various ways, how to create custom gradients and how to use stock patterns and create our own custom patterns. We will also learn how to modify the stroke style (e.g. make it dashed) and how to benefit from markers.

.. toctree::

   palette
   fill-and-stroke-dialog
   custom-colors
   copying-a-color
   creating-gradients
   creating-a-mesh-gradient
   using-patterns
   modifying-patterns
   creating-custom-patterns
   strokes
   markers
   ordering-markers-stroke-and-fill
   custom-markers
