.. Inkscape Beginners' Guide documentation master file, created by
   sphinx-quickstart on Mon Oct 15 00:10:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Inkscape Beginners' Guide's documentation!
=====================================================

.. toctree::
   :numbered: 1
   :maxdepth: 2
   :caption: Table of Contents:
   :name: maintoc

   section_introduction
   section_installing_inkscape
   section_getting_around
   section_draw_with_shapes
   section_free_drawing
   section_color
   section_text
   section_advanced_techniques
   section_beyond_this_guide



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
