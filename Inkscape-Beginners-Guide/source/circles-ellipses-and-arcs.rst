Circles, Ellipses and Arcs
==========================

|Ellipse Tool Icon|

The Ellipse tool can be enabled by clicking the icon shown above, or by
pressing either the F5 or E key. To draw a circle or ellipse, drag the
mouse diagonally, using the same motion as when dragging a selection
box. The circle will appear immediately after you release the mouse
button. To draw a perfect circle, hold down the Ctrl key while you drag
the mouse. Holding Shift will draw starting from the center of the
shape.

The Ellipse tool also allows you to draw arcs and circle segments (or
"pie wedges"). To draw an arc, grab the round handle and drag it, always
keeping the mouse pointer on the inside of the (imaginary) circle.

To draw a segment ("pie wedge"), drag the round handle, always keeping
the mouse pointer on the outside of the (imaginary) circle.

After the first drag, you'll see the second round handle appear. You can
set a specific angle for these shapes on the control bar, using the
"Start" and "End" fields. Note that the three buttons to the right of
those fields do not become activated until after you have dragged the
circle handles.

|Ellipse Tool - Mouse Usage|

*Dragging the square handles converts a circle into an ellipse, while
the round handles make it possible to convert the shape into an arc or
segment ("pie wedge"), depending on the position of the mouse (inside or
outside the imaginary circle) as you drag the handle. The "Start" and
"End" fields, on the control bar indicate the angles between which the
pie or arc extends.*

To quickly restore the circle/ellipse shape, click the far right icon in
the control bar (shown below).

|Button that makes ellipses and circles whole again|

To convert an ellipse into a perfect circle, click on one of the square
handles while pressing Ctrl. The top and left square handles change the
size of the ellipse in vertical and horizontal direction, respectively.

To create a circle with a specific size, switch to the Select tool, and
use the "H" and "W" fields on the control bar, choosing units from the
dropdown menu.

.. |Ellipse Tool Icon| image:: images/ellipse_tool_icon.png
   :width: 50px
   :height: 50px
.. |Ellipse Tool - Mouse Usage| image:: images/ellipse_tool_w_mouse_win10.png
   :width: 500px
   :height: 704px
.. |Button that makes ellipses and circles whole again| image:: images/make_ellipse_whole.png
   :width: 30px
   :height: 30px
