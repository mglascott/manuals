Shape Tools
===========

Geometric shapes, despite their simple look, are useful for many drawing
techniques (and not only vector drawing). With path operations (Union,
Difference, etc…), you can quickly get awesome results from some
elementary shapes. You can even further improve that with path tools.
Both path operations and path tools are detailed in later sections.

|image0|

Let's draw some geometric shapes. All the shape tools follow similar
rules to let you create and edit your shapes. But each tool has specific
options: for example, a circle can become a pie wedge, or a rectangle
can have its corners rounded.

To create a geometric shape:

-  enable the relevant tool from the toolbar (by clicking on it);
-  press the mouse button and hold, while you drag the mouse;
-  release the mouse button to display the shape.

|image1|

*Click and drag to create a shape.*

Once the mouse is released and the shape is displayed, various handles
will become visible. Many, if not all of Inkscape's tools use handles,
for one purpose or another. But it's the handles of the geometric shapes
which are used for creating many fancy and exciting effects. The handles
may be tiny circles, squares and/or diamonds. Sometimes, the same
handles can be available for different tools. We will learn more about
each handle's function in the following chapters.

|image2|

*Primitive shapes provide handles as squares, circles or diamonds.*

Many features of Inkscape are accessible through keyboard shortcuts, and
sometimes only through key shortcuts. While drawing your shape:

-  press the Ctrl key while you drag the mouse, with the Rectangle and
   Ellipse tools, to create squares and circles;
-  press the Shift key while you drag to draw from the center, rather
   than from one corner.

Try drawing some shapes, with and without those keys to get an idea of
how they can be used.

|image3|

*Sketching a cloud from ellipses.*

.. |image0| image:: images/shape-tools-icons.png
.. |image1| image:: images/shape-tool-click-and-drag_2.png
.. |image2| image:: images/shape-tools-handles_1.png
.. |image3| image:: images/ellipses-cloud-example.png

