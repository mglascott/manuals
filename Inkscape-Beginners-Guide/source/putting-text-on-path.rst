Putting Text on a Path
======================

The adjustment of letters' and words' positions are merely the finishing
touch. More often, it is useful to start out by layouting the text as a
whole in a drawing. Inkscape offers two options for this, which are
accessible via Text > Put on Path and Text > Flow into Frame.

|image0|

To make a text follow a path's curvature, you need to do this:

-  1. Create a path that will serve as the base line for the words and
   letters of the text. The path can be created by using the shape
   tools, and combining shapes with the Boolean operations, or by
   drawing it directly with one of the tools that draw paths.
-  2. Write the text directly on the canvas, after clicking on it with
   the mouse. It's not useful to use flowed text in a frame for this.
-  3. With the Selector tool, select the path and the text. Then make
   Inkscape do the work by selecting Text > Put on Path from the menu.

The result will show up on the canvas immediately. The text can still be
edited any time: You can change its contents or its style whenever you
like. The path will remain editable, too: You can shape its curves, add
or delete nodes, move it etc.

Very often, this path only serves to position the text. If you delete
it, the text doesn't have anything to follow anymore, and reverts to its
previous shape. If you don't want to see the path in your drawing,
remove its fill and its stroke!

 

|image1|

 

|image2| 

.. |image0| image:: images/icon-texte-chemin.png
   :width: 30px
   :height: 30px
.. |image1| image:: images/illu_texte5_A.png
   :width: 863px
   :height: 439px
.. |image2| image:: images/illu_texte5_B.png
   :width: 452px
   :height: 436px
