Inkscape Beginners' Guide
=========================

This is the collaborative repository for the Inkscape Beginners' Guide, an introductory practical manual for people who are new to Inkscape.

It is based on the book '`Initiation à Inkscape <https://fr.flossmanuals.net/initiation-inkscape/>`_' by Elisa de Castro Guerra.

To build it locally, you need to have `Sphinx <www.sphinx-doc.org/>`_ installed. For being able to export to pdf, you also need `LaTeX <https://www.latex-project.org/>`_.

The book is licensed CC-By-SA.

Building the Guide
------------------

To build a set of html files (generated index file is ``build/html/index.html``), run:

.. code-block:: none

  make html
  
To build a pdf file (generated file is ``build/latex/InkscapeBeginnersGuide.pdf``), run:

.. code-block:: none

  make latexpdf
