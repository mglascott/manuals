# Beginer Gide Beta Renderer Configuration

Here are the steps to correctly fill all steps of this renderer publish

## Publish

Go to publish tab, select the last radio option: "Try out our new beta renderer for Books".
Use custom radio buton and press settings. 
There is also a "Publish this book" that use the previous stored settings, but need to be selected the radio options mentioned at the begining of this section.

### Settings

The settings are stored in the server and are the same for all users.
If tweak anyone please update this README.

#### Book Formatted PDF Settings

##### 1

Selected: COMICBOOK(168mm x 260mm)
Custom Size: 168 and 260

##### 2

Body Text: Liberation Serif · 13
Heading 1: Liberation Serif · 17 · UPPERCASE · Heavy
Heading 2: Liberation Serif · 15 · UPPERCASE · Heavy
Heading 3: Liberation Serif · 13 · UPPERCASE · Heavy
Formatted: Monospace · 13

##### 3

Not sure how it works couldent render a cover yet

##### 4

###### General

Check "Override Server Default CSS"

###### Page Margins and Gutter

Top Margin 10
Side Margin 15
Bottom Margin 15
Gutter 10
Number of columns 1
Column margin 0

###### Advanced (style sheets)

Textarea: "Your Custom CSS"
Put the CSS on this repo

Textarea: "Booktype CSS (be careful if changing)"
Not change

Checkbox: "Override ALL previous settings with custom CSS"
Checked

##### 5 Submit and store settings
